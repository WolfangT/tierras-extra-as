#!/usr/bin/env python
# -*- coding: utf-8 -*-

from TE.globales import *

from logging import getLogger
REG = getLogger(__name__)

from uuid import uuid4
from os import path

from pygame import image, transform

from TE.faltas import *


class Juego(object):
    """Clase principal del Juego, interfaz por donde controlar todos los componentes"""

    _turno = 0

    def __init__(self, mapa, personajes):
        self.mapa = mapa
        self.listaPersonajes = listaPersonajes(personajes)

    def buscarPersonaje(self, codigo):
        for personaje in self.personajes:
            if hash(personaje) == hash(codigo):
                return personaje
        else:
            return None

    @property
    def personajes(self):
        return self.listaPersonajes.personajes

    @property
    def personajesActivos(self):
        return self.listaPersonajes.personajesActivos

    @property
    def numeroTurno(self):
        return self._turno

    @property
    def numeroPeriodo(self):
        return self.listaPersonajes.nivel + 1


class Personaje(object):
    """Clase base para personajes"""

    _imagen = None
    activo = True

    def __init__(self, nombre, resistencia=0, energia=0, agilidad=0, efecto=None, arte=None, carta=None):
        self.nombre = nombre
        self._uuid = int(uuid4())

        self._resistencia = resistencia
        self._energia = energia
        self._agilidad = agilidad

        self.efecto = efecto
        self.recursos = []
        self.habilidades = []

        self.arte = arte
        self.carta = carta

    def acoplar(self, carta):
        mapeo_max = {
            'Resistencia':self._resistencia,
            'Energia':self._energia,
            'Agilidad':self._agilidad}
        if carta.tipo == 'Recurso':
            if carta.nivel > self.carta.nivel:
                raise NoSuficienteNivel('\
Un Personaje nivel {} no puede equiparse un Recurso nivel {}'.format(self.carta.nivel, carta.nivel))
            maximo = mapeo_max[carta.subtipo]
            actual = self.obtenerAtributo(carta.subtipo)
            if (actual + carta.valor) <= maximo:
                self.recursos.append(carta)
            else:
                raise LimiteReacurso('El personaje a alcansado el limite de {} equipable'.format(carta.subtipo))
        elif carta.tipo == 'Habilidad':
            self.habilidades.append(carta)

    def desactivarRecursos(self, tipo, cantidad=1):
        cantidad = abs(cantidad)
        n = 0
        if cantidad == 0: return
        for carta in self.recursos:
            if carta.subtipo == tipo and carta.activa:
                carta.activa = False
                n += carta.valor
            if n >= cantidad:
                break
        else:
            raise NoSuficientesRecursos('{} no tiene {} {}'.format(self.nombre, cantidad, tipo))

    def destruirRecursos(self, tipo, cantidad=1):
        cantidad = abs(cantidad)
        n = 0
        cartas = []
        for carta in self.recursos:
            if carta.subtipo == tipo:
                cartas.append(carta)
                n += carta.valor
            if n >= cantidad:
                break
        else:
            raise NoSuficientesRecursos('{} no tiene {} {}'.format(self.nombre, cantidad, tipo))
        for carta in cartas:
            del self.recursos[self.recursos.index(carta)]
        return cartas

    def obtenerAtributo(self, atributo):
        if atributo == 'Resistencia':
            return self.resistencia
        if atributo == 'Energia':
            return self.energia
        if atributo == 'Agilidad':
            return self.agilidad

    def obtenerMaxAtributo(self, atributo):
        if atributo == 'Resistencia':
            return self._resistencia
        if atributo == 'Energia':
            return self._energia
        if atributo == 'Agilidad':
            return self._agilidad

    @property
    def cartas(self):
        return self.recursos + self.habilidades + [self.carta]

    @property
    def resistencia(self):
        valor = 0
        for carta in self.recursos:
            if carta.subtipo == 'Resistencia' and carta.activa:
                valor += carta.valor
        return valor

    @property
    def energia(self):
        valor = 0
        for carta in self.recursos:
            if carta.subtipo == 'Energia' and carta.activa:
                valor += carta.valor
        return valor

    @property
    def agilidad(self):
        valor = 0
        for carta in self.recursos:
            if carta.subtipo == 'Agilidad' and carta.activa:
                valor += carta.valor
        return valor

    @property
    def imagen(self):
        if self._imagen:
            return self._imagen
        else:
            self._imagen = transform.scale(
                image.load(path.join(CA, self.arte)),
                (125, 182))
            return self._imagen

    @property
    def numeroMaxRecursos(self):
        return self._resistencia + self._energia + self._agilidad

    def __getstate__(self):
        self._imagen = None
        return self.__dict__

    def __str__(self):
        return '{}(R:{}, E:{}, A:{})'.format(self.nombre, self.resistencia, self.energia, self.agilidad)

    def __hash__(self):
        return self._uuid

    def __eq__(self, objeto):
        if type(objeto) is type(self):
            if hash(objeto) == hash(self):
                return True
            else:
                return False
        else:
            return NotImplemented

    def __ne__(self, objeto):
        return not self.__eq__(objeto)

    def __bool__(self):
        return self.activo


class listaPersonajes(object):
    """Contenedor que mantiene el orden de jugada"""

    nivel = 0

    def __init__(self, personajes):
        self.personajes = personajes

    def __iter__(self):
        """Reorganiza a los personajes por nivel de energia"""
        self.nivel, periodos = self._calcular_orden()
        return self

    def __next__(self):
        if self.nivel < 0:
            raise StopIteration
        else:
            nivel, periodos = self._calcular_orden()
            personajes = periodos[self.nivel]
            self.nivel -= 1
            return personajes

    def _calcular_orden(self):
        personajes = [
            (personaje.energia, personaje)
            for personaje in self.personajes
            if personaje.activo]
        try:
            maximo = max([personaje[0] for personaje in personajes])
        except ValueError:
            maximo = 0
        periodos = {}
        for periodo in range(maximo, -1, -1):
            periodos[periodo] = [
                personaje[1]
                for personaje in personajes
                if personaje[0] >= periodo]
        return maximo, periodos

    @property
    def personajesActivos(self):
        maximo, periodos = self._calcular_orden()
        try:
            return periodos[self.nivel + 1]
        except KeyError:
            return []
