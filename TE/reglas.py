#!/usr/bin/env python
# -*- coding: utf-8 -*-

from TE.globales import *

from logging import getLogger
REG = getLogger(__name__)

from ast import literal_eval
from uuid import uuid4
from importlib import import_module
from copy import copy

from TE.faltas import *
from TE.juego import Juego, Personaje
from TE.mapa import Mapa, Pos
from TE.cartas import BuscarCarta, Carta, MasoCartas
import TE.habilidades


class Reglas(object):
    """Clase principal, mantiene las reglas"""

    def __init__(self, juego, jugadores):
        self.juego = juego
        self.jugadores = jugadores
        self.personajesRequeridos = []

    def turno(self):
        self.juego._turno += 1
        for jugador in self.jugadores:
            jugador.tomarCartas()
        for personaje in self.juego.personajes:
            for carta in personaje.recursos:
                carta.activa = True
            personaje.carta.activa = True

    def periodo(self):
        #~ for personaje in self.personajesRequeridos:
            #~ personaje.destruirRecursos('Energia')
            #~ personaje.carta.activa = True
        pass

    def jugadoresActivos(self):
        jugadores = []
        for jugador in self.jugadores:
            personajes = []
            for personaje in jugador.personajes:
                personaje = self.juego.buscarPersonaje(personaje)
                if personaje:
                    personajes.append(personaje)
            if personajes:
                jugadores.append(jugador)
        return jugadores

    def terminado(self):
        jugadores = self.jugadoresActivos()
        if len(jugadores) == 1:
            return jugadores[0]
        else:
            return False

    def personajesTurno(self):
        self.ocupaciones = {}
        for personaje in [hash(p) for p in self.juego.personajes]:
            self.ocupaciones[personaje] = NADA
        return self.juego.listaPersonajes

    def personajesPeriodo(self, personajes):
        self.personajesRequeridos = []
        for personaje in [hash(personaje) for personaje in personajes]:
            if self.ocupaciones[personaje] == PERIODO:
                self.ocupaciones[personaje] = NADA
            if self.ocupaciones[personaje] == NADA:
                self.personajesRequeridos.append(personaje)
        return self.personajesRequeridos

    def periodoTerminado(self):
        for personaje in self.personajesRequeridos:
            if (self.ocupaciones[personaje] == NADA and
                self.juego.buscarPersonaje(personaje).activo):
                return False
        return True

    @property
    def personajesOcupados(self):
        personajes = []
        for personaje in [hash(p) for p in self.juego.personajes]:
            if not self.ocupaciones[personaje] == NADA:
                personajes.append(personaje)
        return personajes

    def procesarJugadas(self, accion):
        REG.info('{} tomo accion {}'.format(accion.personaje, accion))
        personaje = self.juego.buscarPersonaje(accion.personaje)

        if self.ocupaciones[accion.personaje] in (PERIODO, TURNO):
            raise PersonajeOcupado('{} esta en estado {}'.format(accion.personaje, self.ocupaciones[accion.personaje]))
        elif personaje not in self.juego.personajesActivos:
            raise PersonajeOcupado('{} No esta activo en este Periodo'.format(accion.personaje))
        elif accion.tipo in ('esperar', 'movimiento', 'acoplar', 'habilidad'):
            self.ocupaciones[accion.personaje] = accion.duracion

        if accion.tipo == 'esperar':
            pass

        elif accion.tipo == 'movimiento':
            self.procesarMovimiento(accion)

        elif accion.tipo == 'acoplar':
            self.procesarAcoplar(accion)

        elif accion.tipo == 'habilidad':
            accion(self)

    def procesarAcoplar(self, accion):
        for jugador in self.jugadores:
            for p in [self.juego.buscarPersonaje(p) for p in jugador.personajes]:
                if hash(p) == accion.personaje and accion['carta'].tipo == 'Recurso':
                    if p.resistencia > 0:
                        p.acoplar(accion['carta'])
                        del jugador.mano[accion['pos']]
                        p.desactivarRecursos('Resistencia')
                    elif p.resistencia == 0 and p.carta.activa:
                        p.acoplar(accion['carta'])
                        del jugador.mano[accion['pos']]
                        p.carta.activa = False
                    else:
                        raise NoSuficientesRecursos('{} no tiene suficientes Resistencia'.format(p))
                elif hash(p) == accion.personaje and accion['carta'].tipo == 'Habilidad':
                    p.acoplar(accion['carta'])
                    del jugador.mano[accion['pos']]

    def procesarMovimiento(self, accion):
        pos = self.juego.mapa.buscar(accion.personaje)
        personaje = self.juego.buscarPersonaje(accion.personaje)
        objetivo = copy(accion['objetivo'])
        objetivo.z += 1
        agilidad = personaje.agilidad+1 if personaje.carta.activa else personaje.agilidad

        # Verificacion
        if accion['objetivo'] is None:
            raise Falta('Debes Seleccionar un cuadro')

        cuadros = Distancia(self.juego.mapa, pos, agilidad)[1:]
        if objetivo not in cuadros:
            raise Falta('No es un espacio valido')

        distancia = int(objetivo - pos)
        rutas = Alcanse(self.juego.mapa, pos, distancia)
        for ruta in rutas:
            if ruta[-1] == objetivo:
                pasos = len(ruta)-1

        if personaje.agilidad >= pasos:
            self.juego.mapa.mover(accion.personaje, objetivo)
            personaje.desactivarRecursos('Agilidad', pasos)
        elif personaje.agilidad+1 == pasos and personaje.carta:
            self.juego.mapa.mover(accion.personaje, objetivo)
            personaje.desactivarRecursos('Agilidad', pasos-1)
            personaje.carta.activa = False
        else:
            raise NoSuficientesRecursos('{} no tiene suficientes Agilidad'.format(personaje))


class Jugador(object):
    """Clase base para los Jugadores"""

    def __init__(self, maso, personajes):
        self.maso = maso
        self.mano = []
        self.personajes = list(personajes)
        self._uuid = int(uuid4())

    def tomarCartas(self):
        cartas = self.maso.tomar(2*len(self.personajes))
        c = []
        for carta in cartas:
            if not carta is None:
                c.append(carta)
        self.mano.extend(c)

    def __hash__(self):
        return self._uuid

    def __eq__(self, objeto):
        if type(objeto) is type(self):
            if hash(objeto) == hash(self):
                return True
            else:
                return False
        else:
            return NotImplemented


class Accion(object):
    """Objeto que representa una accion que un personaje realiza"""

    def __init__(self, tipo, duracion, personaje, variables={}):
        """tipo: 'TipoDeAccion', duracion: 'NADA|PERIODO|TURNO',variables: {TIPOEVENTO:('valorDeseado1','valorDesaeo2')}"""
        self.tipo == tipo
        self.duracion = duracion
        self.personaje = personaje
        self.variables = variables
        self.valores = {}

    def agregarVariable(self, evento):
        if evento.type == CAMPOSELECCIONADO:
            if not evento.objetivo is None:
                self.valores['objetivo'] = evento.objetivo
        elif evento.type == CARTASELECCIONADA:
            if not (evento.carta is None or evento.pos is None):
                self.valores['carta'] = evento.carta
                self.valores['pos'] = evento.pos
        elif evento.type == PERSONAJESELECCIONADO:
            if evento.personaje is not None and evento.objetivo is not None:
                self.valores['objetivo'] = evento.objetivo
                self.valores['personajes'] = evento.personaje

    def listo(self):
        l = True
        for variable in self.variables:
            if variable not in self.valores:
                l = False
        return l

    def __getitem__(self, llave):
        return self.valores[llave]

class Esperar(Accion):
    """Subclase de Accion especifica del comando esperar"""

    tipo = 'esperar'
    duracion = PERIODO

    def __init__(self, personaje):
        self.personaje = personaje
        self.variables = {}
        self.valores = {}

class Movimiento(Accion):
    """Subclase de Accion especifica del comando moverse"""

    tipo = 'movimiento'
    #TODO
    duracion = NADA

    def __init__(self, personaje):
        self.personaje = personaje
        self.variables = {'objetivo'}
        self.valores = {}

class Acoplar(Accion):
    """Subclase de Accion especifica del comando acoplar carta"""

    tipo = 'acoplar'
    duracion = NADA

    def __init__(self, personaje):
        self.personaje = personaje
        self.variables = {'carta', 'pos'}
        self.valores = {}

class Habilidad(Accion):
    """Subclase de Accion genreal para las habilidades de carta"""

    tipo = 'habilidad'

    def __init__(self, jugador, personaje, habilidad):
        self.jugador = jugador
        self.personaje = personaje
        self.habilidad = habilidad
        habilidad = import_module('.'+habilidad, package='TE.habilidades')
        self.efecto = habilidad.efecto
        self.variables = habilidad.variables
        self.duracion = habilidad.duracion
        self.costos = habilidad.costos
        self.extras = habilidad.extras
        self.valores = {}

    def verificarCostos(self, reglas):
        personaje = reglas.juego.buscarPersonaje(self.personaje)
        for costo in self.costos:
            if personaje.obtenerAtributo(costo) < self.costos[costo]:
                return False
        else:
            return True

    def verificarExtras(self, reglas):
        for extra in self.extras:
            if extra == 'Rango':
                origen = reglas.juego.mapa.buscar(self.personaje)
                if int(origen-self.valores['objetivo']) > self.extras[extra]:
                    return False
        else:
            return True

    def cobrarCostos(self, reglas):
        personaje = reglas.juego.buscarPersonaje(self.personaje)
        jugador = [jugador for jugador in reglas.jugadores if self.personaje in jugador.personajes][0]
        for costo in self.costos:
            cartas = personaje.destruirRecursos(costo, self.costos[costo])
            jugador.maso.agregar(cartas)

    def __call__(self, reglas):
        if not self.verificarCostos(reglas):
            raise NoSuficientesRecursos('Personaje {} no tiene suficientes recursos'.format(self.personaje))
        if not self.verificarExtras(reglas):
            raise NoSuficienteRango('Esa habilidad no puede ser usada de esa forma')
        self.cobrarCostos(reglas)
        self.efecto(self.jugador, self.personaje, reglas, self.valores)


#Areas de Efecto
def Alcanse(mapa, origen,  distancia):
    """Regresa un lista de caminos posibles del mapa que empiesen en origen
y sean menor o igual de largo que distancia"""
    try:
        padre, origen = origen
    except TypeError:
        padre = None
    if (padre is not None) and not mapa.caminable(origen):
        return
    cuadrosAdyacentes = (
        origen + Pos(1,0),
        origen + Pos(0,1),
        origen + Pos(-1,0),
        origen + Pos(0,-1),
        )
    if distancia > 0:
        caminos = []
        for pos in cuadrosAdyacentes:
            if pos == padre:
                continue
            r = Alcanse(mapa, (origen, pos), distancia-1)
            try:
                for camino in r:
                    camino.insert(0, origen)
                    caminos.append(camino)
            except TypeError:
                if not r is None:
                    caminos.append([origen, r])
        if len(caminos) > 0:
            return caminos
        else:
            return origen
    else:
        return origen

def Distancia(mapa, origen, distancia):
    """Regresa un lista de cuadros que esten a distancia o menos cuadros del origen"""
    cuadros = []
    procesar = [(origen, 0), ]
    while procesar:
        c, d = procesar.pop(0)
        if c != origen and (d > distancia or not mapa.caminable(c)):
            continue
        cuadros.append(c)
        cuadrosAdyacentes = (
            c + Pos(1,0),
            c + Pos(0,1),
            c + Pos(-1,0),
            c + Pos(0,-1),
            )
        for cuadro in cuadrosAdyacentes:
            if cuadro not in cuadros:
                procesar.append((cuadro, d+1))
    return cuadros


# Utilidad
def crearPersonaje(carta, nombre=None):
    """Crea un objeto juego.Personaje apratir de un objeto cartas.Carta"""
    if issubclass(type(carta), Carta) and carta.tipo == 'Personaje':
        p = Personaje(nombre if nombre else carta.nombre,
            carta.salud,
            carta.consentracion,
            carta.movilidad,
            carta.efecto,
            carta.imagen.archivo,
            carta)
        return p
    else:
        raise TypeError('Se necesita un objeto Carta de tipo Personaje')
