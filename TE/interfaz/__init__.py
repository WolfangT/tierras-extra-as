#!/usr/bin/env python
# -*- coding: utf-8 -*-

from TE.globales import *

from logging import getLogger
REG = getLogger(__name__)

from sys import exit
from os import path
from random import randint
from math import copysign
from copy import copy

from pygame.locals import *
from pygame import init, quit, event, Surface, Rect, image, transform, time, display, sprite
init()

from TE.faltas import *
from TE.mapa import Pos
from TE.recursos import obtenerImagen
from TE.reglas import Reglas, Distancia, Accion, Movimiento, Esperar, Acoplar, Habilidad
from TE.interfaz.cargadorRecursos import cargadorTileset
from TE.interfaz.campo import Campo, animacionMovimiento
from TE.interfaz.medidorPersonaje import MedidorPersonaje
from TE.interfaz.zonaCartas import ZonaCartas
from TE.interfaz.menuPersonaje import MenuPersonaje
from TE.interfaz.expansorCarta import ExpansorCarta


OBJETO_ARRASTRADO = None
MOUSE = (0, 0)

class Camara(object):
    """Guarda la informacion de la 'camara', la parte del campo que se ve"""

    def __init__(self):
        self._centro = [VENTANA_ANCHO/2, VENTANA_ALTO/2]
        self._variacion = [0, 0]

    @property
    def dx(self):
        return self._variacion[0]

    @dx.setter
    def dx(self, valor):
        self._variacion[0] = valor

    @property
    def dy(self):
        return self._variacion[1]

    @dy.setter
    def dy(self, valor):
        self._variacion[1] = valor

    @property
    def x(self):
        return self._centro[0]

    @x.setter
    def x(self, valor):
        self._centro[0] = valor

    @property
    def y(self):
        return self._centro[1]

    @y.setter
    def y(self, valor):
        self._centro[1] = valor

    def actualizar(self, campo):
        self._procesar()
        mx = self.x - (campo.ANCHO/2)
        my = self.y - (campo.ALTO/2)
        for sprite in campo.sprites():
            sprite.camara = (mx, my)

    def _procesar(self):
        if abs(self.dx) > 1:
            self.dx = int(copysign(1, self.dx))
        if abs(self.dy) > 1:
            self.dy = int(copysign(1, self.dy))
        self.x += self.dx * VELOCIDAD_CAMARA
        self.y += self.dy * VELOCIDAD_CAMARA


class Interfaz(object):

    AREAS = []
    AREAS_VIEJAS = []
    AREAS_ANTIGUAS = []

    Fondo = None
    Campo = None
    MedidoresPersonajes = None
    ZonaCartas = None
    ExpansorCarta = None
    MenuPersonaje = None

    CartaSeleccionada = None
    PersonajeSeleccionado = None
    ImagenArrastrada = None

    def __init__(self):
        self.Reloj = time.Clock()
        self.Pantalla = display.set_mode(
            (VENTANA_ANCHO, VENTANA_ALTO),
            HWSURFACE|DOUBLEBUF)
        self.Camara = Camara()
        self.Pantalla.fill(NEGRO)
        self.AREAS.append(self.Pantalla.get_rect())

    def bucleGUI(self):
        self.bucleEventos()
        #Lista de cosas que hacer
        self.DibujarCampo()
        self.DibujarPosibilidades()
        self.DibujarCartas()
        self.DibujarHUD()
        self.DibujarMenu()
        self.DibujarExpansor()
        self.DibujarArrastre()
        #Determinar areas de la pantalla de repintar
        if self.AREAS:
            areas = self.AREAS + self.AREAS_VIEJAS + self.AREAS_ANTIGUAS
            self.AREAS_ANTIGUAS = self.AREAS
        else:
            areas = self.AREAS + self.AREAS_VIEJAS
        display.update(areas)
        self.AREAS_VIEJAS = self.AREAS
        self.AREAS = []

    def bucleEventos(self):
        global VENTANA_ALTO, VENTANA_ANCHO, MOUSE
        for evento in event.get():
            # envetos del sistema
            if evento.type == QUIT:
                quit()
                exit()
            # Eventos de Teclas
            elif evento.type == KEYDOWN:
                if evento.key in (K_UP, K_w):
                    self.Camara.dy += 1
                if evento.key in (K_DOWN, K_s):
                    self.Camara.dy -= 1
                if evento.key in (K_RIGHT, K_d):
                    self.Camara.dx -= 1
                if evento.key in (K_LEFT, K_a):
                    self.Camara.dx += 1
            elif evento.type == KEYUP:
                if evento.key in (K_UP, K_w):
                    self.Camara.dy = 0
                if evento.key in (K_DOWN, K_s):
                    self.Camara.dy = 0
                if evento.key in (K_RIGHT, K_d):
                    self.Camara.dx = 0
                if evento.key in (K_LEFT, K_a):
                    self.Camara.dx = 0
            # Eventos del Raton
            elif evento.type == MOUSEMOTION:
                MOUSE = evento.pos
            elif evento.type == MOUSEBUTTONDOWN:
                MOUSE = evento.pos
                self.precionar(evento)
            elif evento.type == MOUSEBUTTONUP:
                MOUSE = evento.pos
                self.soltar(evento)
            elif evento.type == ARRASTRAR:
                self.arrastrar(evento)
            # Eventos del juego
            elif evento.type == PERSONAJESELECCIONADO:
                if self.ACCION_ACTIVA:
                    self.ACCION_ACTIVA.agregarVariable(evento)
                    continue
                if evento.tipo == 'secundario':
                    self.PersonajeSeleccionado = evento.personaje
            elif evento.type == CAMPOSELECCIONADO:
                if self.ACCION_ACTIVA:
                    self.ACCION_ACTIVA.agregarVariable(evento)
                    continue
            elif evento.type == CARTASELECCIONADA:
                if self.ACCION_ACTIVA:
                    self.ACCION_ACTIVA.agregarVariable(evento)
                    continue
                if evento.tipo == 'secundario':
                    self.CartaSeleccionada = evento.carta
            elif evento.type == COMANDOSELECCIONADO:
                if evento.comando == 'Cancelar' or evento.comando is None:
                    self.ACCION_ACTIVA = None
                    self.PersonajeSeleccionado = None
                elif evento.comando == 'Esperar':
                    self.ACCION_ACTIVA = Esperar(evento.personaje)
                    self.PersonajeSeleccionado = None
                else:
                    self.ACCION_ACTIVA = Habilidad(
                        self.jugador,
                        evento.personaje,
                        evento.comando)

    def _determinarEvento(self, evento):
        """Determina que tipo de evento genera el seleccionar algo"""
        boton = evento.button
        cor = evento.pos
        evento = None
        tipo = {
            1 : 'principal',
            2 : None,
            3 : 'secundario',
            4 : None,
            5 : None,
            }
        if self.ExpansorCarta and self.ExpansorCarta.rect.collidepoint(*cor):
            x = cor[0] - self.ExpansorCarta.rect.left
            y = cor[1] - self.ExpansorCarta.rect.top
            self.ExpansorCarta.precionar(x,y)

        elif self.MenuPersonaje and self.MenuPersonaje.rect.collidepoint(*cor):
            cartaHabilidad = self.seleccionarMenu(cor)
            if cartaHabilidad is not None:
                if cartaHabilidad in ('Esperar', 'Cancelar'):
                    evento = event.Event(
                        COMANDOSELECCIONADO,
                        comando=cartaHabilidad,
                        carta=None,
                        personaje=hash(self.MenuPersonaje.personaje),
                        cordenadas=cor,
                        tipo=tipo[boton],
                        )
                else:
                    evento = event.Event(
                        COMANDOSELECCIONADO,
                        comando=cartaHabilidad.efecto.titulo,
                        carta=cartaHabilidad,
                        personaje=hash(self.MenuPersonaje.personaje),
                        cordenadas=cor,
                        tipo=tipo[boton],
                        )
        elif self.ZonaCartas and self.ZonaCartas.rect.collidepoint(*cor):
            carta, pos = self.seleccionarCarta(cor)
            if (not carta is None) and (not pos is None):
                evento = event.Event(
                    CARTASELECCIONADA,
                    carta=carta,
                    pos=pos,
                    cordenadas=cor,
                    tipo=tipo[boton]
                    )
        elif self.Campo:
            objetivo = self.Campo.buscarCuadro(*cor)
            if objetivo is not None:
                if objetivo.z == 0:
                    evento = event.Event(
                        CAMPOSELECCIONADO,
                        objetivo=objetivo,
                        cordenadas=cor,
                        tipo=tipo[boton]
                        )
                elif objetivo.z == 1:
                    evento = event.Event(
                        PERSONAJESELECCIONADO,
                        personaje=hash(self.juego.mapa[objetivo]),
                        objetivo=objetivo,
                        cordenadas=cor,
                        tipo=tipo[boton]
                        )
        return evento

    def precionar(self, evento):
        '''Maneja eventos MOUSEBUTTONDOWN del sistema y crea eventos del juego'''
        global OBJETO_ARRASTRADO
        evento = self._determinarEvento(evento)
        if evento and evento.tipo == 'principal':
            OBJETO_ARRASTRADO = evento
            #prepararse para arrastrar el objeto por la pantalla
            if OBJETO_ARRASTRADO.type == CARTASELECCIONADA:
                imagen = transform.scale(
                    OBJETO_ARRASTRADO.carta.png,
                    (CARTA_ANCHO, CARTA_ALTO),
                    )
                self.ImagenArrastrada = animacionMovimiento([imagen])
            elif OBJETO_ARRASTRADO.type == PERSONAJESELECCIONADO:
                personaje = self.juego.buscarPersonaje(OBJETO_ARRASTRADO.personaje)
                if hash(personaje) in self.jugador.personajes:
                    self.ImagenArrastrada = animacionMovimiento(
                        self.Campo.tileset[
                            '{}@moverse'.format(personaje.nombre)])
        elif evento and evento.tipo == 'secundario':
            event.post(evento)

    def soltar(self, evento):
        '''Maneja eventos MOUSEBUTTONUP del sistema y crea eventos del juego'''
        global OBJETO_ARRASTRADO
        if OBJETO_ARRASTRADO is None:
            return
        evento = self._determinarEvento(evento)
        if evento:
            i = Pos(*OBJETO_ARRASTRADO.cordenadas)
            f = Pos(*evento.cordenadas)
            if float(i-f) > 10:
                arrastrar = event.Event(
                    ARRASTRAR,
                    inicial=OBJETO_ARRASTRADO,
                    final=evento,
                    )
                event.post(arrastrar)
            else:
                event.post(OBJETO_ARRASTRADO)
        OBJETO_ARRASTRADO = None
        self.ImagenArrastrada = None
        self.limpiarPantalla()

    def arrastrar(self, evento):
        '''Maneja los eventos virtuales ARRASTRAR y crea eventos del juego'''
        global OBJETO_ARRASTRADO
        a = None
        e = []
        if evento.inicial.type == CARTASELECCIONADA:
            if evento.final.type == PERSONAJESELECCIONADO:
                personaje = self.juego.buscarPersonaje(evento.final.personaje)
                if hash(personaje) not in self.jugador.personajes:
                    return
                a = Acoplar(hash(personaje))
                e = [evento.inicial,]
        elif evento.inicial.type == PERSONAJESELECCIONADO:
            personaje = self.juego.buscarPersonaje(evento.inicial.personaje)
            if personaje and evento.final.type == CAMPOSELECCIONADO:
                if hash(personaje) not in self.jugador.personajes:
                    return
                a = Movimiento(hash(personaje))
                e = [evento.final,]

        self.ACCION_ACTIVA = a
        for evento in e:
            event.post(evento)

    def seleccionarCarta(self, mouse):
        '''seleciona una carta para mostrar'''
        x = mouse[0] - self.ZonaCartas.rect.left
        y = mouse[1] - self.ZonaCartas.rect.top
        return self.ZonaCartas.buscarCarta(x, y)

    def seleccionarMenu(self, mouse):
        '''seleciona una carta para mostrar'''
        x = mouse[0] - self.MenuPersonaje.rect.left
        y = mouse[1] - self.MenuPersonaje.rect.top
        return self.MenuPersonaje.precionar(x, y)

    def limpiarPantalla(self):
        """limpia toda la pantalla"""
        self.Campo._clip = self.Pantalla.get_rect()
        self.Campo.repaint_rect(self.Pantalla.get_rect())

    def DibujarArrastre(self):
        if self.ImagenArrastrada is None:
            return
        else:
            imagen = next(self.ImagenArrastrada)
            self.limpiarPantalla()
        if OBJETO_ARRASTRADO.type == CARTASELECCIONADA:
            rect = imagen.get_rect(center=MOUSE)
            x, y = rect.topleft
            temp = Surface(rect.size).convert()
            temp.blit(self.Pantalla, (-x, -y))
            temp.blit(imagen, (0,0))
            temp.set_alpha(128)
            self.Pantalla.blit(temp, rect)
        elif OBJETO_ARRASTRADO.type == PERSONAJESELECCIONADO:
            rect = imagen.get_rect(midbottom=MOUSE)
            self.Pantalla.blit(imagen, rect)
            self.Campo.desactivar(OBJETO_ARRASTRADO.personaje)
        self.AREAS.append(rect)

    def DibujarPosibilidades(self):
        if OBJETO_ARRASTRADO:
            if OBJETO_ARRASTRADO.type == PERSONAJESELECCIONADO:
                personaje = self.juego.buscarPersonaje(
                    OBJETO_ARRASTRADO.personaje)
                if hash(personaje) in self.jugador.personajes:
                    agilidad = (personaje.agilidad + 1
                        if personaje.carta else personaje.agilidad)
                    cuadros = Distancia(
                        self.juego.mapa,
                        OBJETO_ARRASTRADO.objetivo,
                        agilidad,
                        )
                    self.Campo.seleccionar(*cuadros[1:])

    def DibujarCampo(self):
        if self.Campo is not None:
            self.Campo.actualizar(self.reglas, self.jugador)
            self.Camara.actualizar(self.Campo)
            self.Campo.clear(self.Pantalla, self.Campo.fondo)
            areas = self.Campo.draw(self.Pantalla)
            self.AREAS.extend(areas)
        elif self.juego is not None:
            self.Campo = Campo(self.juego.mapa)

    def DibujarCartas(self):
        if self.ZonaCartas is not None:
            areas = self.ZonaCartas.actualizar(self.jugador)
            self.ZonaCartas.rect.bottomleft = (MARGEN, VENTANA_ALTO-MARGEN)
            self.Pantalla.blit(self.ZonaCartas, self.ZonaCartas.rect)
            for area in areas:
                area.top += self.ZonaCartas.rect.top
                area.left += self.ZonaCartas.rect.left
            self.AREAS.extend(areas)
        elif self.jugador is not None:
            self.ZonaCartas = ZonaCartas(self.jugador)

    def DibujarHUD(self):
        if self.MedidoresPersonajes is not None:
            for n, codigo in enumerate(self.jugador.personajes):
                personaje = self.juego.buscarPersonaje(codigo)
                areas = self.MedidoresPersonajes[n].actualizar(personaje)
                self.MedidoresPersonajes[n].rect.topleft = (MARGEN, MARGEN + n*(ALTO_MEDIDOR + MARGEN))
                self.Pantalla.blit(self.MedidoresPersonajes[n], self.MedidoresPersonajes[n].rect)
                for area in areas:
                    area.top += self.MedidoresPersonajes[n].rect.top
                    area.left += self.MedidoresPersonajes[n].rect.left
                self.AREAS.extend(areas)
        elif self.jugador is not None and self.juego is not None:
            m = []
            for n, codigo in enumerate(self.jugador.personajes):
                personaje = self.juego.buscarPersonaje(codigo)
                m.append(MedidorPersonaje(personaje))
            self.MedidoresPersonajes = m

    def DibujarMenu(self):
        if self.juego and self.MenuPersonaje is not None:
            personaje = self.juego.buscarPersonaje(self.PersonajeSeleccionado)
            if (self.PersonajeSeleccionado and
                hash(personaje) in self.jugador.personajes):
                areas = self.MenuPersonaje.actualizar(personaje)
                self.MenuPersonaje.rect.midtop = (VENTANA_ANCHO/2, MARGEN)
                self.Pantalla.blit(self.MenuPersonaje, self.MenuPersonaje.rect)
                for area in areas:
                    area.top += self.MenuPersonaje.rect.top
                    area.left += self.MenuPersonaje.rect.left
                self.AREAS.extend(areas)
            else:
                self.AREAS.append(self.MenuPersonaje.rect)
                self.MenuPersonaje = None
                self.limpiarPantalla()
        elif self.juego and self.PersonajeSeleccionado is not None:
            personaje = self.juego.buscarPersonaje(self.PersonajeSeleccionado)
            if hash(personaje) in self.jugador.personajes:
                self.MenuPersonaje = MenuPersonaje(personaje)

    def DibujarExpansor(self):
        if self.ExpansorCarta is not None:
            if self.ExpansorCarta.activo:
                areas = self.ExpansorCarta.actualizar(self.CartaSeleccionada)
                self.ExpansorCarta.rect.midright = (
                    VENTANA_ANCHO - MARGEN,
                    (VENTANA_ALTO - ZONA_CARTA_ALTO) / 2)
                self.Pantalla.blit(self.ExpansorCarta, self.ExpansorCarta)
                for area in areas:
                    area.top += self.ExpansorCarta.rect.top
                    area.left += self.ExpansorCarta.rect.left
                self.AREAS.extend(areas)
            else:
                self.AREAS.append(self.ExpansorCarta.rect)
                self.ExpansorCarta = None
                self.CartaSeleccionada = None
                self.limpiarPantalla()
        elif self.CartaSeleccionada is not None:
            self.ExpansorCarta = ExpansorCarta(self.CartaSeleccionada)
