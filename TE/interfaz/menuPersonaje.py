#!/usr/bin/env python
# -*- coding: utf-8 -*-

from TE.globales import *

from logging import getLogger
REG = getLogger(__name__)

from copy import copy
from collections import deque

from pygame.locals import *
from pygame import sprite, Surface, transform

from TE.recursos import obtenerImagen


class MenuPersonaje(Surface, sprite.LayeredDirty):
    """Widget que muestra la habilidades de un personaje"""

    def __init__(self, personaje):
        self.personaje = personaje
        self.marco = obtenerImagen('Marco.png')
        Surface.__init__(
            self,
            (ANCHO_MENU, ALTO_MENU),
            flags=SRCALPHA,
            )
        sprite.LayeredDirty.__init__(self, _time_threshold=1000)
        self.rect = self.get_rect()
        #sprites habilidades
        self.n = 0
        self.crearSprites(personaje)

    def crearSprites(self, personaje):
        self.add(ExtI(), ExtD())
        self.add(SepI(), SepD())
        habilidades = [
            carta for carta in personaje.cartas
            if carta.efecto is not None]
        habilidades.insert(0, 'Esperar')
        habilidades.insert(0, 'Cancelar')
        for i in range(-1,2):
            self.add(Habilidad(habilidades, self.n, i))

    def actualizar(self, personaje):
        self.personaje = personaje
        habilidades = [
            carta for carta in personaje.cartas
            if carta.efecto is not None]
        habilidades.insert(0, 'Esperar')
        habilidades.insert(0, 'Cancelar')
        self.update(habilidades, self.n)
        return self.draw(self)

    def precionar(self, x, y):
        for sprite in self.sprites():
            if sprite.rect.collidepoint(x,y):
                if type(sprite) is ExtI:
                    self.n -= 1
                if type(sprite) is ExtD:
                    self.n += 1
                if type(sprite) is Habilidad:
                    return sprite.habilidad


class Habilidad(sprite.DirtySprite):

    def __init__(self, habilidades, n, POS):
        self.POS = POS
        sprite.DirtySprite.__init__(self)
        self.rect = Rect((14 + ((self.POS+1) * (4+40)), 0), (40, 40))
        #recursos
        self.frente = obtenerImagen('Marco.png')
        self.fondo = obtenerImagen('Marco_F.png')
        self.icono = None
        self.iconoCancelar = obtenerImagen('Cancelar.png', (40, 40))
        self.iconoPasar = obtenerImagen('Dormir.png', (40, 40))
        self.superficie = Surface(self.rect.size).convert_alpha()
        #calculo inicial
        self.determinarHabilidad(habilidades, n)

    @property
    def image(self):
        self.superficie.fill(TRANSPARENTE)
        self.superficie.blit(self.fondo, (0,0))
        #TODO imagen habilidad
        if self.icono:
            self.superficie.blit(self.icono, (0,0))
        self.superficie.blit(self.frente, (0,0))
        return self.superficie

    def determinarHabilidad(self, habilidades, n):
        self.habilidades = copy(habilidades)
        self.n = n
        habilidades = deque(self.habilidades)
        habilidades.rotate(-self.n)
        try:
            if len(habilidades) != 1:
                self.habilidad = habilidades[self.POS]
            else:
                h = copy(habilidades)
                h.append(None)
                self.habilidad = h[self.POS]
        except IndexError:
            self.habilidad = None
        if self.habilidad:
            REG.debug('Cargando habilidad: {}'.format(self.habilidad))
            if self.habilidad == 'Esperar':
                self.icono = self.iconoPasar
            elif self.habilidad == 'Cancelar':
                self.icono = self.iconoCancelar
            else:
                REG.debug('Cargando el icono de la habilidad {}'\
                .format(self.habilidad.efecto.titulo))
                self.icono = obtenerImagen(
                    '{}.png'.format(self.habilidad.efecto.titulo),
                    (40, 40))
        else:
            self.icono = None

    def update(self, habilidades, n):
        if habilidades != self.habilidades or n != self.n:
            self.determinarHabilidad(habilidades, n)
            self.dirty = 1


class ExtI(sprite.DirtySprite):

    def __init__(self):
        sprite.DirtySprite.__init__(self)
        #atributos de sprite
        self.image = obtenerImagen('Marco_BI.png')
        self.rect = self.image.get_rect(midleft=(0,ALTO_MENU/2))

class ExtD(sprite.DirtySprite):
    """Sprite que controla el boton para cerrar"""

    def __init__(self):
        sprite.DirtySprite.__init__(self)
        #atributos de sprite
        self.image = obtenerImagen('Marco_BD.png')
        self.rect = self.image.get_rect(midright=(ANCHO_MENU,ALTO_MENU/2))

class SepI(sprite.DirtySprite):
    """Sprite que controla el boton para cerrar"""

    def __init__(self):
        sprite.DirtySprite.__init__(self)
        #atributos de sprite
        self.image = obtenerImagen('Marco_S.png')
        self.rect = self.image.get_rect(midleft=(14+40,ALTO_MENU/2))

class SepD(sprite.DirtySprite):
    """Sprite que controla el boton para cerrar"""

    def __init__(self):
        sprite.DirtySprite.__init__(self)
        #atributos de sprite
        self.image = obtenerImagen('Marco_S.png')
        self.rect = self.image.get_rect(midright=(ANCHO_MENU-(14+40),ALTO_MENU/2))
