#!/usr/bin/env python
# -*- coding: utf-8 -*-

from TE.globales import *

from logging import getLogger
REG = getLogger(__name__)

from copy import copy

from pygame.locals import *
from pygame import sprite, Surface, Rect, transform

from TE.recursos import obtenerImagen, FONDO_CARTA


class ZonaCartas(Surface, sprite.LayeredDirty):
    """Objeto que representa el lugar donde van el mazo y la mano de cartas"""

    def __init__(self, jugador):
        Surface.__init__(self, (ZONA_CARTA_ANCHO, ZONA_CARTA_ALTO), flags=SRCALPHA)
        self.rect = self.get_rect()
        sprite.LayeredDirty.__init__(self, _time_threshold=100)
        self.fondo = obtenerImagen('FondoZonaCartas.png', self.rect.size)
        self.clear(self, self.fondo)
        self.jugador = jugador
        self.crearSprites(jugador)

    def crearSprites(self, jugador):
        self.mano = ManoCartas(jugador)
        self.maso = MasoCartas(jugador)
        self.add(self.mano, self.maso)

    def actualizar(self, jugador):
        self.jugador = jugador
        self.update(jugador)
        if jugador.mano != self.mano.mano:
            self._clip = Rect((0,0), (VENTANA_ANCHO, VENTANA_ALTO))
            self.repaint_rect(self.get_rect())
        return self.draw(self)

    def buscarCarta(self, X, Y):
        if not self.mano.rect.collidepoint(X, Y):
            return None, None
        X -= self.mano.rect.left
        Y -= self.mano.rect.top
        sep = (
            (
                (CARTA_ANCHO * len(self.mano.mano)) -
                (MANO_ANCHO - (8*MARGEN))
                ) /
            (len(self.mano.mano) - 1)
            ) if (len(self.mano.mano) - 1) > 0 else 0
        for c in range(len(self.jugador.mano)-1, -1, -1):
            if sep < 0:
                x = (3 * MARGEN) + (c * ((MARGEN / 2) + CARTA_ANCHO))
            else:
                x = (3 * MARGEN) + (c * (CARTA_ANCHO - sep))
            y = (ZONA_CARTA_ALTO - CARTA_ALTO) / 2
            a = Rect((x, y), (CARTA_ANCHO, CARTA_ALTO))
            if a.collidepoint(X, Y):
                return self.jugador.mano[c], c
        return None, None


class MasoCartas(sprite.DirtySprite):
    """Sprite que represeta un maso de cartas boca abajo"""

    def __init__(self, jugador):
        sprite.DirtySprite.__init__(self)
        #atributos de sprite
        self.superficie = Surface(self.rect.size).convert_alpha()
        self.cartas = (CARTAS_MASO * len(jugador.maso)
            // jugador.maso.tamano)

    @property
    def rect(self):
        return Rect((MANO_ANCHO, 0), (MASO_ANCHO, ZONA_CARTA_ALTO))

    @property
    def image(self):
        self.superficie.fill(TRANSPARENTE)
        extra = (MARGEN/2) * (CARTAS_MASO-1)
        a = Rect((0, 0), (FONDO_ANCHO + extra, FONDO_ALTO + extra))
        a.center = (self.rect.width/2, self.rect.height/2)
        for l in range(self.cartas):
            x = a.left + ((MARGEN/2) * l)
            y = a.top + ((MARGEN/2) * (CARTAS_MASO-l-1))
            self.superficie.blit(
                obtenerImagen(
                    FONDO_CARTA,
                    (FONDO_ANCHO, FONDO_ALTO)),
                (x, y))
        return self.superficie

    def update(self, jugador):
        cartas = (CARTAS_MASO * len(jugador.maso)
            // jugador.maso.tamano)
        if cartas != self.cartas:
            self.dirty = 1
            self.cartas = cartas


class ManoCartas(sprite.DirtySprite):
    """Sprite que represeta un maso de cartas boca abajo"""

    def __init__(self, jugador):
        sprite.DirtySprite.__init__(self)
        #atributos de sprite
        self.superficie = Surface(self.rect.size).convert_alpha()
        self.mano = jugador.mano

    @property
    def rect(self):
        return Rect((0, 0), (MANO_ANCHO, ZONA_CARTA_ALTO))

    @property
    def image(self):
        self.superficie.fill(TRANSPARENTE)
        if len(self.mano):
            sep = (((CARTA_ANCHO * len(self.mano)) -
                    (MANO_ANCHO - (8 * MARGEN))
                    ) / (len(self.mano) - 1)
                ) if (len(self.mano) - 1) > 0 else 0
            for c in range(len(self.mano)):
                if sep < 0:
                    x = (3 * MARGEN) + (c * ((MARGEN/2) + CARTA_ANCHO))
                else:
                    x = (3 * MARGEN) + (c * (CARTA_ANCHO - sep))
                png = transform.scale(
                    self.mano[c].png,
                    (CARTA_ANCHO, CARTA_ALTO),
                    )
                self.superficie.blit(
                    png,
                    png.get_rect(
                        topleft=(x, (ZONA_CARTA_ALTO-CARTA_ALTO) / 2))
                    )
        return self.superficie

    def update(self, jugador):
        if jugador.mano != self.mano:
            self.dirty = 1
            self.mano = copy(jugador.mano)
