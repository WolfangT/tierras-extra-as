#!/usr/bin/env python
# -*- coding: utf-8 -*-

from TE.globales import *

from logging import getLogger
REG = getLogger(__name__)

from pygame.locals import *
from pygame import Rect

from configparser import ConfigParser

from TE.recursos import obtenerConf, obtenerImagen


class cargadorRecursos(object):
    """Objeto que actua como intefase entre el sistema y las imagenes"""
    pass


class cargadorTileset(cargadorRecursos):
    """Especialisado en sistemas basados en tilesets"""

    def __init__(self, archivoConf):
        self.config = obtenerConf(archivoConf)
        self.sets = {}
        self.ancho = self.config.getint('tilesets', 'ancho')
        self.alto = self.config.getint('tilesets', 'alto')
        for set in self.config.get('tilesets', 'sets').split(','):
            self.sets[set] = tileset(self.config, set)

    def __getitem__(self, tile):
        if ':' in tile:
            nombre, bordes = tile.split(':', 1)
            bordes = set(bordes.split(','))
            animaciones = None
        elif '@' in tile:
            nombre, animaciones = tile.split('@', 1)
            bordes = None
        else:
            nombre = tile
            bordes = None
            animaciones = None
        #determinar tile estandar
        tileset = self.config.get(nombre, 'tileset')
        tiles = self.config.get(nombre, 'cor').split(';')
        #determinar tile deacuerdo a sus bordes
        if bordes and self.config.getboolean(nombre, 'bordes'):
            opciones = self.config.options(nombre)
            for opcion in opciones:
                if 'borde_' in opcion:
                    direcciones = set(opcion[6:].split(','))
                    if direcciones == bordes:
                        tiles = self.config.get(nombre, opcion).split(';')
        #determinar tiles de una animacion
        elif animaciones and self.config.getboolean(nombre, 'animable'):
            opciones = self.config.options(nombre)
            for opcion in opciones:
                if 'animacion_' in opcion:
                    if animaciones == opcion[10:]:
                        tiles = self.config.get(nombre, opcion).split(';')
        return [self.sets[tileset][int(cor.split(',')[0])][int(cor.split(',')[1])] for cor in tiles]


class tileset(list):
    """Cargador de tilesets"""

    def __init__(self, config, tileset):
        list.__init__(self)
        #Variables
        opciones = dict(config.items('tileset_{}'.format(tileset)))
        self.imagen = obtenerImagen(opciones['archivo'])
        self.ancho_imagen, self.alto_imagen = self.imagen.get_size()
        self.alto = int(opciones['alto'])
        self.ancho = int(opciones['ancho'])
        #Cargar cuadros
        for x in range(0, self.ancho_imagen//self.ancho):
            fila = []
            for y in range(0, self.alto_imagen//self.alto):
                rect = Rect((x*self.ancho, y*self.alto), (self.ancho, self.alto))
                fila.append(self.imagen.subsurface(rect))
            self.append(fila)
