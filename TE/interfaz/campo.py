#!/usr/bin/env python
# -*- coding: utf-8 -*-

from TE.globales import *

from logging import getLogger
REG = getLogger(__name__)

from copy import copy

import sys
from os import path
from random import randint

from pygame.locals import *
from pygame import init, event, Surface, Rect, image, transform, time, display, sprite
init()

from TE.mapa import Estatico, Movil
from TE.interfaz.cargadorRecursos import cargadorTileset
from TE.recursos import obtenerImagen


class Campo(sprite.LayeredDirty):
    """Widget que representa el campo de juego"""

    def __init__(self, mapa):
        sprite.LayeredDirty.__init__(self, _time_threshold=1000000)
        #TODO Buscar mejor sistema
        self.tileset = cargadorTileset('Floating_Islands.tileset')
        self.ANCHO = mapa.totX * self.tileset.ancho * 2
        self.ALTO = (mapa.totY + 2) * self.tileset.alto * 2
        self.fondo = obtenerImagen(
            'Fondo_{}.png'.format(mapa.fondo),
            (VENTANA_ANCHO, VENTANA_ALTO),
            )
        self.selectores = []
        self.crearSprites(mapa)

    def crearSprites(self, mapa):
        for pos, objeto in mapa.lista():
            if type(objeto) is Movil:
                self.add(Personaje(objeto, pos, self.tileset), layer=Personaje.layer)
            elif type(objeto) is Estatico:
                if objeto.altura:
                    self.add(ParedFrente(objeto, pos, self.tileset, mapa), layer=ParedFrente.layer)
                    self.add(ParedTope(objeto, pos, self.tileset, mapa), layer=ParedTope.layer)
                else:
                    self.add(Bloque(objeto, pos, self.tileset, mapa), layer=Bloque.layer)
                self.add(Fondo(objeto, pos, self.tileset, mapa), layer=Fondo.layer)
            else:
                self.add(Sprite(objeto, pos, self.tileset, mapa))

    def seleccionar(self, *posiciones):
        """Iluminar ciertos cuadros por este loop"""
        self.selectores = [Selector(pos, self.tileset) for pos in posiciones]
        for sprite in self.selectores:
            self.add(sprite, layer=Selector.layer)

    def desactivar(self, *personajes):
        """Por este loop agregar transparencia aciertos persanajes"""
        for personaje in personajes:
            for sprite in self.sprites():
                if hash(sprite) == hash(personaje):
                    sprite.seleccionable = False

    def actualizar(self, reglas, jugador):
        self.update(reglas, jugador)

    def buscarCuadro(self, x, y):
        sprites = self.get_sprites_at((x,y))
        if sprites:
            return sprites[-1].pos

    def draw(self, *args, **kwargs):
        arg = sprite.LayeredDirty.draw(self, *args, **kwargs)
        for selector in self.selectores:
            selector.kill()
        return arg


class Sprite(sprite.DirtySprite):
    """Clase Base para los elementos visuales del juego"""

    def __init__(self, Objeto, pos, tileset, mapa):
        '''Elemento visual del juego que representa a un mapa.Objeto'''
        sprite.DirtySprite.__init__(self)
        self.rect = Rect((0,0), (tileset.ancho*2, tileset.alto*2))
        #atributos de sprite
        self._uuid = Objeto._uuid
        self.nombre = Objeto.nombre
        self.pos = pos
        self.tileset = tileset
        self.altura = Objeto.altura
        self.ver = None
        #acciones
        self.calcularPos()

    @property
    def image(self):
        imagenes = self.tileset[self.nombre]
        if self.ver is None:
            self.ver = randint(0,len(imagenes)-1)
        imagen = imagenes[self.ver]
        return imagen

    _camara = (0,0)

    @property
    def camara(self):
        return self._camara

    @camara.setter
    def camara(self, valor):
        if valor != self._camara:
            self.dirty = 1
            self._camara = valor
            self.calcularPos()

    def __hash__(self):
        return self._uuid

    def calcularPos(self):
        x = self.pos.x * self.tileset.ancho * 2
        y = (self.pos.y + 1) * self.tileset.alto *  2
        self.rect.topleft = (self.camara[0] + x, self.camara[1] + y)


class Bloque(Sprite):
    """Tipo de Sprite analogo a Estatico, representa un bloque de un material"""

    layer = 1

    def __init__(self, Objeto, pos, tileset, mapa):
        Sprite.__init__(self, Objeto, pos, tileset, mapa)
        self.superficie = Surface(self.rect.size).convert_alpha()
        self.direcciones = None
        self.ver = [None, None, None, None]
        #funciones
        self.calcularDirecciones(mapa)

    @property
    def image(self):
        return self.superficie

    def calcularDirecciones(self, mapa):
        self.direcciones = set()
        for direccion, objeto in list(mapa.obtenerAlrededor(self.pos).items()):
            if objeto is None or self.nombre not in objeto.nombre:
                self.direcciones.add(direccion)
        self.calcularBordes()

    def calcularBordes(self):
        self.superficie.fill(TRANSPARENTE)
        bordes = (None, None, None, None)
        for d, b in list(BORDES_BLOQUES.items()):
            if set(d) == self.direcciones:
                bordes = b
        for p, borde in enumerate(bordes):
            if borde is None:
                nombre = self.nombre
            else:
                nombre = '{}:{}'.format(self.nombre, ','.join(borde))
            imagenes = self.tileset[nombre]
            if self.ver[p] is None:
                self.ver[p] = randint(0, len(imagenes)-1)
            imagen = imagenes[self.ver[p]]
            centro = (self.rect.width/2, self.rect.height/2)
            if p == 0:
                rect = imagen.get_rect(bottomright=centro)
            elif p == 1:
                rect = imagen.get_rect(bottomleft=centro)
            elif p == 2:
                rect = imagen.get_rect(topright=centro)
            elif p == 3:
                rect = imagen.get_rect(topleft=centro)
            self.superficie.blit(imagen, rect)


class Fondo(Bloque):
    """Parte que representa adornos abajo de campo"""

    layer = 0

    def calcularPos(self):
        x = self.pos.x * self.tileset.ancho * 2
        y = (self.pos.y + 2) * self.tileset.alto *  2
        self.rect.topleft = (self.camara[0] + x, self.camara[1] + y)

    def calcularDirecciones(self, mapa):
        self.direcciones = set()
        for direccion, objeto in list(mapa.obtenerAlrededor(self.pos).items()):
            if objeto is None:
                self.direcciones.add(direccion)
        self.calcularBordes()

    def calcularBordes(self):
        self.superficie.fill(TRANSPARENTE)
        bordes = (None, None, None, None)
        try:
            self.direcciones.remove(N)
        except KeyError:
            pass
        for d, b in list(BORDES_BLOQUES_FONDO.items()):
            if set(d) == self.direcciones:
                bordes = b
        n = 'Fondo'
        if S in bordes or (S,O) in bordes or (S,E) in bordes:
            for p, borde in enumerate(bordes):
                if borde is None:
                    nombre = n
                else:
                    nombre = '{}:{}'.format(n, ','.join(borde))
                imagenes = self.tileset[nombre]
                if self.ver[p] is None:
                    self.ver[p] = randint(0, len(imagenes)-1)
                imagen = imagenes[self.ver[p]]
                centro = (self.rect.width/2, self.rect.height/2)
                if p == 0:
                    rect = imagen.get_rect(bottomright=centro)
                elif p == 1:
                    rect = imagen.get_rect(bottomleft=centro)
                elif p == 2:
                    rect = imagen.get_rect(topright=centro)
                elif p == 3:
                    rect = imagen.get_rect(topleft=centro)
                self.superficie.blit(imagen, rect)
        return self.superficie


class ParedFrente(Bloque):
    """Sprite con altura y que bloquea la vista"""

    layer = 1

    def calcularBordes(self):
        self.superficie.fill(TRANSPARENTE)
        dir = {d for d in (E,O) if d in self.direcciones}
        bordes = (None, None, None, None)
        for d, b in list(BORDES_BLOQUES.items()):
            if set(d) == dir:
                bordes = b
        if S in self.direcciones:
            centro = (self.rect.width/2, self.rect.height/2)
            for p, borde in enumerate(bordes):
                if borde is None:
                    nombre = self.nombre
                else:
                    nombre = '{}:{}'.format(self.nombre, ','.join(borde))
                imagenes = self.tileset[nombre]
                if not self.ver[p]:
                    self.ver[p] = randint(0,len(imagenes)-1)
                imagen = imagenes[self.ver[p]]
                if p == 0:
                    rect = imagen.get_rect(topright=centro)
                elif p == 1:
                    rect = imagen.get_rect(topleft=centro)
                self.superficie.blit(imagen, rect)


class ParedTope(Bloque):
    """Sprite con altura y que bloquea la vista"""

    layer = 3

    def calcularPos(self):
        x = self.pos.x * self.tileset.ancho * 2
        y = ((self.pos.y + 1) * 2*self.tileset.alto) - self.tileset.alto
        self.rect.topleft = (self.camara[0] + x, self.camara[1] + y)

    def calcularBordes(self):
        self.superficie.fill(TRANSPARENTE)
        bordes = (None, None, None, None)
        for d, b in list(BORDES_BLOQUES.items()):
            if set(d) == self.direcciones:
                bordes = b
        n = self.nombre[:-6] if '_Pared' in self.nombre else self.nombre
        for p, borde in enumerate(bordes):
            if borde is None:
                nombre = n
            else:
                nombre = '{}:{}'.format(n, ','.join(borde))
            imagenes = self.tileset[nombre]
            if self.ver[p] is None:
                self.ver[p] = randint(0, len(imagenes)-1)
            imagen = imagenes[self.ver[p]]
            centro = (self.rect.width/2, self.rect.height/2)
            if p == 0:
                rect = imagen.get_rect(bottomright=centro)
            elif p == 1:
                rect = imagen.get_rect(bottomleft=centro)
            elif p == 2:
                rect = imagen.get_rect(topright=centro)
            elif p == 3:
                rect = imagen.get_rect(topleft=centro)
            self.superficie.blit(imagen, rect)


class Personaje(Sprite):
    """Tipo de Sprte analogo a Movil, representa un personaje"""

    layer = 2

    def __init__(self, Objeto, pos, tileset):
        '''Elemento visual del juego que representa a un mapa.Objeto'''
        sprite.DirtySprite.__init__(self)
        self.rect = Rect((0, 0), (tileset.ancho*2, tileset.alto*3))
        #atributos de sprite
        self._uuid = Objeto._uuid
        self.nombre = Objeto.nombre
        self.pos = pos
        self.tileset = tileset
        #atributos de personaje
        self.superficie = Surface(self.rect.size).convert_alpha()
        self.animacion = None
        self.selector = None
        self.seleccionable = True
        self.selectores = {
            'normal' : self.tileset['Selector'][0],
            'inactivo' : None,
            }

    def update(self, reglas, jugador):
        # Estandar
        personajes = [hash(p) for p in reglas.juego.personajesActivos]
        personaje = reglas.juego.buscarPersonaje(self)
        mapa = reglas.juego.mapa
        # Mover de posicion
        try:
            pos = mapa.buscar(self)
            if pos != self.pos:
                self.dirty = 1
                self.pos = copy(pos)
        except KeyError: #Empujado fuera del mapa
            self.visible = 0
            self.dirty = 1
        else:
            self.calcularPos()
        # Determinar selector
        if (personaje.activo and
            hash(personaje) in jugador.personajes and
            hash(personaje) in personajes and
            not hash(personaje) in reglas.personajesOcupados
            ):
            self.selector = 'normal'
            animacion = 'extra'
        else:
            self.selector = 'inactivo'
            animacion = 'normal'
        # Determinar animacion
        if not reglas.juego.buscarPersonaje(self).activo:
            animacion = 'morir'
        if animacion != self.animacion:
            nombre = '{}@{}'.format(self.nombre, animacion)
            if animacion == 'normal':
                self.imagen = animacionNormal(self.tileset[nombre])
            elif animacion == 'extra':
                self.imagen = animacionExtra(self.tileset[nombre])
            elif animacion == 'morir':
                self.imagen = animacionMuerte(self.tileset[nombre])
            self.animacion = animacion
        self.dirty = 1

    @property
    def image(self):
        self.superficie.fill(TRANSPARENTE)
        if not self.seleccionable:
            self.seleccionable = True
        else:
            if self.selectores[self.selector]:
                self.superficie.blit(
                    self.selectores[self.selector],
                    self.selectores[self.selector].get_rect(
                        bottom=self.rect.height))
        self.superficie.blit(next(self.imagen), (0,0))
        return self.superficie

    def calcularPos(self):
        x = self.pos.x * self.tileset.ancho * 2
        y = ((self.pos.y + 1) * self.tileset.alto * 2) - self.tileset.alto
        self.rect.topleft = (self.camara[0] + x, self.camara[1] + y)


def animacionNormal(sprites):
    """Funcion generadora que regresa las animacion estandar"""
    m = len(sprites) - 1
    n = 0
    while True:
        yield sprites[n]
        yield sprites[n]
        n += 1
        if n > m:
            n = 0

def animacionExtra(sprites):
    """Funcion generadora que regresa las animacion estandar"""
    m = len(sprites) - 1
    n = 0
    while True:
        yield sprites[n]
        n += 1
        if n > m:
            n = 0

def animacionMovimiento(sprites):
    """Funcion generadora que regresa las animacion al moverse"""
    m = len(sprites) - 1
    n = 0
    while True:
        yield sprites[n]
        n += 1
        if n > m:
            n = 0

def animacionAtaque(sprites):
    """Funcion generadora que regresa las animacion de ataque"""
    for sprite in sprites:
        yield sprite

def animacionMuerte(sprites):
    """Funcion generadora que regresa las animacion al morir"""
    m = len(sprites) - 1
    n = 0
    while True:
        yield sprites[n]
        yield sprites[n]
        yield sprites[n]
        n += 1
        if n > m:
            n = m


class Selector(Sprite):
    """Objeto virtual diseñado para eliminarse cada frame"""

    layer = 2

    def __init__(self, pos, tileset):
        sprite.DirtySprite.__init__(self)
        self.pos = pos
        self.tileset = tileset
        self.rect = Rect((0, 0), (self.tileset.ancho*2, self.tileset.alto*2))
        self.dirty = 2

    @property
    def image(self):
        return self.tileset['Selector'][0]

    def __hash__(self):
        return int(id(self))

    def update(self, reglas, jugador):
        x = self.pos.x * self.tileset.ancho * 2
        y = (self.pos.y + 1) * self.tileset.alto *  2
        self.rect.topleft = (self.camara[0] + x, self.camara[1] + y)
