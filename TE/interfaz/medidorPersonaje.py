#!/usr/bin/env python
# -*- coding: utf-8 -*-

from TE.globales import *

from copy import copy, deepcopy

from logging import getLogger
REG = getLogger(__name__)

from pygame.locals import *
from pygame import sprite, Surface, transform

from TE.recursos import obtenerIconoMedidor, obtenerImagen, \
    BARRA_VIDA_RECURSOS_C, BARRA_VIDA_RECURSOS_D, BARRA_VIDA_IMAGEN


class MedidorPersonaje(Surface, sprite.LayeredDirty):
    """Widget que muestra la informacion de recursos de un personaje"""

    def __init__(self, personaje):
        ancho = ALTO_MEDIDOR + (ICONO_ANCHO*(personaje.numeroMaxRecursos+1))
        Surface.__init__(
            self,
            (ancho, ALTO_MEDIDOR),
            flags=SRCALPHA,
            )
        self.rect = self.get_rect()
        sprite.LayeredDirty.__init__(self, _time_threshold=1000)
        s = Surface(self.get_rect().size).convert_alpha()
        s.fill(TRANSPARENTE)
        self.clear(self, s)

        self.crearSprites(personaje)

    def crearSprites(self, personaje):
        self.add(ImagenPersonaje(personaje))
        for atributo in ('Resistencia', 'Energia', 'Agilidad'):
            self.add(ContadorRecursos(personaje, atributo))
        #~ self.add(InformacionPersonaje(personaje))

    def actualizar(self, personaje):
        self.update(personaje)
        return self.draw(self)


class ImagenPersonaje(sprite.DirtySprite):
    """Sprite que representa la parte del HUD con la imagen del personaje"""

    def __init__(self, personaje):
        sprite.DirtySprite.__init__(self)
        #Imagen Personaje
        self.fondo = obtenerImagen(
            BARRA_VIDA_IMAGEN,
            (ANCHO_IMAGEN, ALTO_MEDIDOR),
            )
        self.imagen = obtenerImagen(
            '{}.png'.format(personaje.nombre),
            (ANCHO_IMAGEN-(MARGEN*2) , ALTO_MEDIDOR-(MARGEN*2)),
            )
        #atributos de sprite
        self.rect = self.fondo.get_rect()
        self.superficie = Surface(self.rect.size).convert_alpha()
        self.personaje = personaje

    @property
    def image(self):
        self.superficie.fill(TRANSPARENTE)
        self.superficie.blit(self.fondo, (0,0))
        self.superficie.blit(
            self.imagen,
            (6*MARGEN/8, MARGEN)
            )
        if not self.personaje.activo:
            self.superficie.fill(MEDIOGRIS)
        return self.superficie

    def update(self, personaje):
        if ((personaje.carta.activa != self.personaje.carta.activa) or
            (personaje.activo != self.personaje.activo)):
            self.dirty = 1
            self.personaje = copy(personaje)


class ContadorRecursos(sprite.DirtySprite):
    """Sprite que representa la parte del HUD con la imagen del personaje"""

    def __init__(self, personaje, atributo):
        sprite.DirtySprite.__init__(self)
        #atributos de sprite
        self.personaje = personaje
        self.ATRIBUTO = atributo
        self.superficie = Surface(self.rect.size).convert_alpha()
        #recursos
        self.fondo = obtenerImagen(
            BARRA_VIDA_RECURSOS_C,
            (ALTO_BARRA, ALTO_BARRA),
            )
        self.fondoF = obtenerImagen(
            BARRA_VIDA_RECURSOS_D,
            (ALTO_BARRA, ALTO_BARRA),
            )

    @property
    def rect(self):
        r = ('Resistencia', 'Energia', 'Agilidad')
        return Rect(
            (ANCHO_IMAGEN, (MARGEN/4)+(ALTO_BARRA*r.index(self.ATRIBUTO))),
            (self.personaje.obtenerMaxAtributo(self.ATRIBUTO)*ALTO_BARRA, ALTO_BARRA),
            )

    @property
    def image(self):
        self.superficie.fill(TRANSPARENTE)
        pos = 0
        n = 0
        for carta in self.personaje.recursos:
            if carta.subtipo == self.ATRIBUTO:
                n += 1
                r = self.personaje.obtenerMaxAtributo(self.ATRIBUTO)-n
                tipo = 'normal' if carta.activa else 'usado'
                self.superficie.blit(
                    self.fondo if r else self.fondoF,
                    (ICONO_ANCHO*pos, 0),
                    )
                self.superficie.blit(
                    obtenerIconoMedidor(self.ATRIBUTO, tipo),
                    (ICONO_ANCHO*pos, MARGEN/2),
                    )
                pos += 1
        for i in range(self.personaje.obtenerMaxAtributo(self.ATRIBUTO)-n):
            n += 1
            r = self.personaje.obtenerMaxAtributo(self.ATRIBUTO)-n
            self.superficie.blit(
                self.fondo if r else self.fondoF,
                (ICONO_ANCHO*pos, 0),
                )
            self.superficie.blit(
                obtenerIconoMedidor(self.ATRIBUTO, 'vacio'),
                (ICONO_ANCHO*pos, MARGEN/2),
                )
            pos += 1
        return self.superficie

    def update(self, personaje):
        if (personaje.obtenerAtributo(self.ATRIBUTO) !=
            self.personaje.obtenerAtributo(self.ATRIBUTO)
            ):
            REG.debug('Recargando HUD')
            REG.debug(personaje)
            REG.debug(self.personaje)
            self.dirty = 1
            self.personaje = deepcopy(personaje)


class InformacionPersonaje(sprite.DirtySprite):
    """Sprite que representa la parte del HUD con la imagen del personaje"""

    def __init__(self, personaje):
        sprite.DirtySprite.__init__(self)
        #Imagen fondo y nombre
        self.nombrePersonaje = LETRA_PEQUENA.render(personaje.nombre, True, NEGRO)
        self.ancho = self.nombrePersonaje.get_width() + ICONO_ANCHO + MARGEN
        self.fondo =  transform.scale(
            BARRA_VIDA_NOMBRE,
            (self.ancho, ALTO_MEDIDOR/2),
            )
        #atributos de sprite
        self.rect = self.fondo.get_rect(topleft=(ALTO_MEDIDOR, ALTO_MEDIDOR/2))
        self.superficie = Surface(self.rect.size).convert_alpha()
        self.personaje = personaje

    @property
    def image(self):
        self.superficie.fill(TRANSPARENTE)
        self.superficie.blit(self.fondo, (0,0))
        self.superficie.blit(self.nombrePersonaje, (MARGEN, MARGEN))
        return self.superficie

    def update(self, personaje):
        pass
