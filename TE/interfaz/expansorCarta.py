#!/usr/bin/env python
# -*- coding: utf-8 -*-

from TE.globales import *

from logging import getLogger
REG = getLogger(__name__)

from copy import copy

from pygame.locals import *
from pygame import sprite, Surface, transform

from TE.recursos import obtenerImagen


class ExpansorCarta(Surface, sprite.LayeredDirty):
    """Objeto que muestra una vercion agrandada de una carta"""

    def __init__(self, carta):
        Surface.__init__(
            self,
            (EXPANSOR_ANCHO, EXPANSOR_ALTO),
            flags=SRCALPHA,
            )
        self.rect = self.get_rect()
        sprite.LayeredDirty.__init__(self, _time_threshold=1000)
        self.fondo = obtenerImagen(
            'FondoCartaAgrandada.png',
            self.rect.size,
            )
        self.clear(self, self.fondo)

        self.crearSprites(carta)
        self.activo = True

    def crearSprites(self, carta):
        self.add(CartaGrande(carta))
        self.boton = BotonCerrar()
        self.add(self.boton)

    def actualizar(self, carta):
        self.update(carta)
        return self.draw(self)

    def precionar(self, x, y):
        if self.boton.rect.collidepoint(x,y):
            self.activo = False


class CartaGrande(sprite.DirtySprite):
    """Sprite que muestra un acarta grande"""

    def __init__(self, carta):
        sprite.DirtySprite.__init__(self)
        self.carta = carta
        #atributos de sprite
        self.image = transform.smoothscale(
            self.carta.png, (CARTAEXP_ANCHO, CARTAEXP_ALTO))
        self.rect = self.image.get_rect(
            bottomleft=(MARGEN, EXPANSOR_ALTO - MARGEN))

    def update(self, carta):
        if carta != self.carta:
            self.dirty = 1
            self.carta = carta
            self.image = transform.smoothscale(
                self.carta.png,
                (CARTAEXP_ANCHO, CARTAEXP_ALTO))


class BotonCerrar(sprite.DirtySprite):
    """Sprite que controla el boton para cerrar"""

    def __init__(self):
        sprite.DirtySprite.__init__(self)
        #atributos de sprite
        self.boton = obtenerImagen(
            'Boton_Cerrar.png',
            (EXPANSOR_ANCHO*7/100, EXPANSOR_ALTO*5/100),
            )
        self.boton_precionado = obtenerImagen(
            'Boton_Cerrar_Precionado.png',
            (EXPANSOR_ANCHO*7/100, EXPANSOR_ALTO*5/100),
            )
        self.boton_desactivo = obtenerImagen(
            'Boton_Cerrar_Desactivo.png',
            (EXPANSOR_ANCHO*7/100, EXPANSOR_ALTO*5/100),
            )
        self.rect = self.boton.get_rect(topleft=(EXPANSOR_ANCHO*89/100, EXPANSOR_ALTO*2/100))
        self.estado = 0
        self.precionar = False

    @property
    def image(self):
        if self.estado == 0:
            return self.boton
        elif self.estado == 1:
            return self.boton_precionado
        elif self.estado == 2:
            return self.boton_desactivado

    def update(self, carta):
        pass
