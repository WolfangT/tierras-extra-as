#!/usr/bin/env python
# -*- coding: utf-8 -*-

from TE.globales import *

from logging import getLogger
REG = getLogger(__name__)

from os.path import join, isfile
from configparser import ConfigParser

from pygame import error


#Fuentes
_CACHE_FUENTES = {}
def obtenerFuente(nombre, tamano):
    archivo = join(CARPETA_FUENTES, nombre)
    fuente = _CACHE_FUENTES.get((archivo, tamano))
    if fuente is None:
        REG.debug('Cargando fuente {}'.format(archivo))
        fuente = Font(archivo, tamano)
        _CACHE_FUENTES[(archivo, tamano)] = fuente
    return fuente

LETRA_PEQUENA   =   obtenerFuente('LinBiolinum_R.otf', 16)
LETRA_MEDIANA   =   obtenerFuente('LinBiolinum_R.otf', 24)
LETRA_GRANDE    =   obtenerFuente('LinBiolinum_R.otf', 32)


#Niveles
_CACHE_NIVELES = {}
def obtenerNivel(nombre):
    archivo = join(CARPETA_NIVELES, '{}.nivel'.format(nombre))
    nivel = _CACHE_NIVELES.get(archivo)
    if nivel == None:
        REG.debug('Cargando nivel {}'.format(archivo))
        nivel = ConfigParser()
        nivel.read(archivo)
        _CACHE_NIVELES[archivo] = nivel
    return nivel


#Guardados
_CACHE_GUARDADOS = {}
def obtenerGuardado(nombre):
    archivo = join(CARPETA_GUARDADOS, '{}.conf'.format(nombre))
    guardado = _CACHE_GUARDADOS.get(archivo)
    if guardado == None:
        REG.debug('Cargando guardado {}'.format(archivo))
        guardado = ConfigParser()
        guardado.read(archivo)
        _CACHE_GUARDADOS[archivo] = guardado
    return guardado


#Configuraciones
_CACHE_CONF = {}
def obtenerConf(archivo):
    archivo = join(CARPETA_CONF, archivo)
    conf = _CACHE_CONF.get(archivo)
    if conf == None:
        REG.debug('Cargando conf {}'.format(archivo))
        conf = ConfigParser()
        conf.read(archivo)
        _CACHE_CONF[archivo] = conf
    return conf


#Imagenes
_CACHE_IMAGENES = {}
def obtenerImagen(nombre, scala=None):
    scala = scala and (int(scala[0]), int(scala[1]))
    for carpeta in (CA, CG, CI, CP):
        archivo = join(carpeta, nombre)
        llave = (archivo, scala)
        imagen = _CACHE_IMAGENES.get(llave)
        if imagen is None:
            if isfile(archivo):
                imagen = load(archivo).convert_alpha()
                REG.debug('Cargando imagen {} a {}'.format(archivo, scala))
            else:
                continue
            if scala is not None:
                imagen = scale(imagen, scala)
            _CACHE_IMAGENES[llave] = imagen
        return imagen
    else:
        REG.warn('No se pudo encontrar {} en ninguna parte'.\
        format(archivo))

def obtenerFondo(nombre):
    archivo = 'Fondo_{}.png'.format(nombre)
    return obtenerImagen(archivo)

def obtenerIconoMedidor(tipo, estado):
    nombre = 'C{}{}.png'.format(tipo.capitalize(), estado.capitalize())
    return obtenerImagen(nombre, (ICONO_ANCHO, ICONO_ALTO))

BARRA_VIDA_RECURSOS_C   =   'BarraVidaMedidorC.png'
BARRA_VIDA_RECURSOS_D   =   'BarraVidaMedidorD.png'
BARRA_VIDA_IMAGEN       =   'BarraVidaImagen.png'

FONDO_CARTA = 'Fondo.png'
