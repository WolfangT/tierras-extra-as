#!/usr/bin/env python
# -*- coding: utf-8 -*-

from TE.globales import *

from logging import getLogger
REG = getLogger(__name__)

from pygame.locals import *
from pygame import init, quit, event, display, time
init()

from albow.root import RootWidget
from albow.theme import root
from albow.controls import Button, Label
from albow.fields import TextField
from albow.layout import Column, Row

from TE.cliente import Cliente, cargarJugador
from TE.servidor import Servidor
from TE.recursos import obtenerFuente, obtenerImagen

class Inicio(object):

    def __init__(self):
        self.Reloj = time.Clock()
        self.Pantalla = display.set_mode(
            (VENTANA_ANCHO, VENTANA_ALTO),
            HWSURFACE|DOUBLEBUF)
        self.dibujarPantalla()

    def dibujarPantalla(self):
        #Elementos
        self.root = RootWidget(self.Pantalla)
        self.root.bg_image = obtenerImagen('Fondo_Nubes.png', (800,600))
        self.BServidor = Button('Iniciar Servidor', action=self.lansarJuego)
        self.BConectarse = Button('Conectarse con el Servidor', action=self.conectarseJuego)
        self.ipServidor = TextField(width='255.255.255.255')
        titulo = Label(
            text='Tierras Extrañas',
            width=300,
            font=obtenerFuente('LinBiolinum_R.otf', 30),
            align='c')

        #Layout
        conectarse = Column((
            self.BConectarse,
            Row((
                Label('IP del Servidor'),
                self.ipServidor))
            ))
        menu = Row((self.BServidor, conectarse), spacing=100)
        pantalla = Column((titulo, menu), spacing=50)
        self.root.add_centered(pantalla)
        self.root.run()

    def lansarJuego(self):
        try:
            servidor = Servidor(2, 'Nivel0')
        except OSError:
            REG.error('Ya existe un Servidor')
            return
        servidor.start()
        jugador = cargarJugador('wolfang')
        juego = Cliente(jugador, 'localhost')
        juego.start()
        servidor.join()

    def conectarseJuego(self):
        ip = self.ipServidor.value
        jugador = cargarJugador('wolfang')
        juego = Cliente(jugador, ip)
        juego.start()

    def bucleGUI(self):
        self.Pantalla.fill(BLANCO)
        display.update()


def main():
    inicio = Inicio()

if __name__ == '__main__':
    main()
