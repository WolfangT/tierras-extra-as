#!/usr/bin/env python
# -*- coding: utf-8 -*-

from TE.globales import *

from logging import getLogger
REG = getLogger(__name__)

from time import sleep
from pickle import dumps, loads
from configparser import ConfigParser, NoOptionError
from os import path
import sys

from PodSixNet.Connection import ConnectionListener, connection

from TE.faltas import *
from TE.recursos import obtenerGuardado
from TE.interfaz import Interfaz
from TE.reglas import Reglas, Jugador, crearPersonaje
from TE.juego import Juego, Personaje
from TE.mapa import Mapa
from TE.cartas import Carta, MasoCartas, BuscarCarta


class Cliente(ConnectionListener, Interfaz):
    """Convinacion de la interfaz y el cliente"""

    PUERTO = 5071

    PERSONAJES_PEDIDOS = []
    ESPERANDO_JUGADAS = False
    ACCIONES = []
    ACCION_ACTIVA = None
    EJECUTAR = True

    juego = None
    jugador = None
    reglas = None

    def __init__(self, jugador, servidor='localhost'):
        ConnectionListener.__init__(self)
        Interfaz.__init__(self)
        servidor = servidor if servidor else 'localhost'
        self.jugador = jugador
        connection.DoConnect((servidor, self.PUERTO))
        REG.info("INICIANDO CONECCION A {}:{}".format(servidor, self.PUERTO))

    def start(self):
        self.EJECUTAR = True
        while self.EJECUTAR:
            self.bucleRed()
            self.bucleGUI()
            self.Reloj.tick(FPS)
            self.verificarAccionActiva()

    def bucleRed(self):
        self.Pump()
        connection.Pump()

    def verificarAccionActiva(self):
        if self.ACCION_ACTIVA and self.ACCION_ACTIVA.listo():
            self.ACCIONES.append(self.ACCION_ACTIVA)
            self.EnviarAccion(self.ACCION_ACTIVA)
            self.ACCION_ACTIVA = None

    # Mensajes del Juego
    def EnviarAccion(self, accion):
        REG.info('Enviando la accion {} al Servidor'.format(accion))
        a = {
            'action' : 'RecivirAccion',
            'accion' : dumps(accion),
            }
        connection.Send(a)

    # Llamas del juego
    def Network_ActualizarJuego(self, datos):
        self.reglas = loads(datos['reglas'])
        self.juego = self.reglas.juego
        self.jugador = self.reglas.jugadores[self.reglas.jugadores.index(self.jugador)]
        if datos['tipo'] == 'turno':
            #~ self.reglas.turno()
            REG.info('Turno {}'.format(self.juego.numeroTurno))
        if datos['tipo'] == 'periodo':
            #~ self.reglas.periodo()
            REG.info('Periodo {}'.format(self.juego.numeroPeriodo))
        if datos['tipo'] == 'terminado':
            REG.info('Juego Terminado, el ganador es {}'.format(self.reglas.terminado()))
            self.EJECUTAR = False

    def Network_RecivirAccion(self, datos):
        accion = loads(datos['accion'])
        self.reglas.procesarJugadas(accion)
        self.ACCIONES.append(accion)

    def Network_PedirJugadas(self, datos):
        personajes = loads(datos['personajes'])
        self.PERSONAJES_PEDIDOS = personajes
        self.ESPERANDO_JUGADAS = True

    # Llamadas del sistema
    def Network_connected(self, datos):
        REG.info('Conectado con el servidor, enviando informacion personal')
        m = {
            'action' : 'EnviarJugador',
            'judador' : self.jugador,
            }
        connection.Send(m)

    def Network_error(self, datos):
        REG.critical('error: {}'.format(datos['error']))
        connection.Close()

    def Network_disconnected(self, datps):
        REG.error('Server disconnected')


def cargarJugador(archivo):
    conf = obtenerGuardado(archivo)
    nombre_jugador = conf.get('Jugador', 'nombre')
    ps = conf.get('Jugador', 'personajes').split(',')
    maso = MasoCartas(10*len(ps))
    personajes = []
    for p in ps:
        sec = 'Personaje_{}'.format(p)
        personajes.append(BuscarCarta(int(conf.get(sec, 'codigo'))))
        maso.agregar(cargarMaso(conf, conf.get(sec, 'maso')))
    return Jugador(maso, personajes)

def cargarMaso(conf, seccion):
    maso = MasoCartas(10)
    seccion = 'Maso_{}'.format(seccion)
    cartas = []
    for n in range(10):
        try:
            c = conf.get(seccion, 'carta{}'.format(n))
            cartas.append(BuscarCarta(int(c)))
        except NoOptionError:
            cartas.append(None)
    maso.agregar(cartas)
    return maso

def inciarCliente(jugador, servidor):
    jugador = cargarJugador(jugador)
    cliente = Cliente(jugador, servidor)
    cliente.start()

def main():
    if len(sys.argv) != 3:
        print(("Uso:", sys.argv[0], "Jugador Servidor"))
        print(("e.g.", sys.argv[0], "wolfang localhost"))
    else:
        conf = sys.argv[1]
        host = sys.argv[2]
        inciarCliente(conf, host)

if __name__ == '__main__':
    main()
