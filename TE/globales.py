#!/usr/bin/env python
# -*- coding: utf-8 -*-

from sys import path
from os.path import abspath, dirname, join
from glob import glob

from pygame import init
init()
from pygame import Color
from pygame.font import Font
from pygame.transform import scale
from pygame.image import load

#Carpetas
CARPETA             = abspath(path[0])
RECURSOS            = join(CARPETA, 'Recursos')
#Sistemas
CARPETA_GUARDADOS   = join(RECURSOS, 'Guardados')
CARPETA_LOGS        = join(RECURSOS, 'Logs')
CARPETA_NIVELES     = join(RECURSOS, 'Niveles')
CARPETA_CONF        = join(RECURSOS, 'Configuraciones')
#recursos
CARPETA_FUENTES     = join(RECURSOS, 'Fuentes')
CARPETA_CURSORES    = join(RECURSOS, 'Cursores')
CARPETA_CARTAS      = join(RECURSOS, 'Cartas')
CA                  = join(RECURSOS, 'Arte')
CI                  = join(RECURSOS, 'Iconos')
CP                  = join(RECURSOS, 'PreGeneradas')
CG                  = join(RECURSOS, 'Graficos')
CS                  = join(RECURSOS, 'Sonidos')
CM                  = join(RECURSOS, 'Musica')

#Archivos Conf Logs
CONF_LOG_SERVIDOR   = join(CARPETA_CONF, 'Servidor.conf')
CONF_LOG_CLIENTE    = join(CARPETA_CONF, 'Cliente.conf')
CONF_LOG_MAIN       = join(CARPETA_CONF, 'Main.conf')

#Colores
TRANSPARENTE        = Color(0,    0,      0,      0)
BLANCO              = Color(255,  255,    255,    255)
NEGRO               = Color(0,    0,      0,      255)
ROJO                = Color(255,  0,      0,      255)
VERDE               = Color(0,    255,    0,      255)
AZUL                = Color(0,    0,      255,    255)
AMARILLO            = Color(255,  255,    0,      255)
MEDIOGRIS           = Color(128,  128,    128,    128)

#Cartas
LICENCIA_CARTA      = "®Tierras extrañas: Escaramusas, por \
Wolfang Torres, Todos los derechos reservados"
LISTA_CARTAS        = glob(join(CARPETA_CARTAS, '*.xml'))

#Interfaz
TITULO              = 'Tierras Extrañas'
FPS                 = 15
VELOCIDAD_CAMARA    = 120//FPS
MARGEN              = 8

#interfaz - dimanico
VENTANA_ANCHO       = 800
VENTANA_ALTO        = 600

#Interface - ZonaCartas
CARTAS_MASO         = 10
ZONA_CARTA_ALTO     = round((VENTANA_ALTO / 4) - (2 * MARGEN))
ZONA_CARTA_ANCHO    = VENTANA_ANCHO - (2 * MARGEN)
CARTA_ALTO          = ZONA_CARTA_ALTO - (2 * MARGEN)
CARTA_ANCHO         = round(56 * CARTA_ALTO / 86)
FONDO_ALTO          = CARTA_ALTO - ((MARGEN / 2) * (CARTAS_MASO + 1))
FONDO_ANCHO         = round(59 * FONDO_ALTO / 86)
MASO_ANCHO          = round(VENTANA_ANCHO / 5)
MANO_ANCHO          = round(4 * VENTANA_ANCHO / 5)

#Interface - Expansor Cartas
EXPANSOR_ALTO       = round(3 * VENTANA_ALTO / 4)
EXPANSOR_ANCHO      = round(59 * (10 * EXPANSOR_ALTO / 11) / 86)
CARTAEXP_ALTO       = round((10 * EXPANSOR_ALTO / 11) - (2 * MARGEN))
CARTAEXP_ANCHO      = round(59 * CARTAEXP_ALTO / 86)

#Interface - Medidor de Recursos
ALTO_MEDIDOR        = 64
ANCHO_IMAGEN        = 68
ALTO_BARRA          = int((ALTO_MEDIDOR - (MARGEN/2)) / 3)
ICONO_ANCHO         = ALTO_BARRA - MARGEN
ICONO_ALTO          = ICONO_ANCHO

#Interface - MenuPersonaje
ALTO_MENU           = 40
ANCHO_MENU          = (2*14) + (2*4) + (3*40)


#Interface - Eventos Virtuales
# Rango 24 - 32 #(El esquema puede cambiar en SDL 1.3)
PERSONAJESELECCIONADO = 24
CAMPOSELECCIONADO = 25
COMANDOSELECCIONADO = 26
CARTASELECCIONADA = 27
ARRASTRAR = 30

#Reglas
NADA = bin(0)
PERIODO = bin(1)
TURNO = bin(2)


#Mapa - Direcciones
N = 'n'
S = 's'
E = 'e'
O = 'o'
NO = 'no'
NE = 'ne'
SO = 'so'
SE = 'se'
BORDES_BLOQUES = {
    (N) : (N,N,None,None),
    (S) : (None,None,S,S),
    (E) : (None,E,None,E),
    (O) : (O,None,O,None),
    (N,S) : (N,N,S,S),
    (E,O) : (O,E,O,E),
    (N,E) : (N,(N,E),None,E),
    (N,O) : ((N,O),N,O,None),
    (S,E) : (None,E,S,(S,E)),
    (S,O) : (O,None,(S,O),S),
    (N,S,E) : (N,(N,E),S,(S,E)),
    (S,E,O) : (O,E,(S,O),(S,E)),
    (N,S,O) : ((N,O),N,(S,O),S),
    (N,E,O) : ((N,O),(N,E),O,E),
    (N,S,E,O) : ((N,O),(N,E),(S,O),(S,E)),
    }
BORDES_BLOQUES_FONDO = {
    (S) : (N,N,S,S),
    (S,E) : (N,(N,E),S,(S,E)),
    (S,O) : ((N,O),N,(S,O),S),
    (S,E,O) : ((N,O),(N,E),(S,O),(S,E)),
    }
