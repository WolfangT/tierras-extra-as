#!/usr/bin/env python
# -*- coding: utf-8 -*-

from TE.globales import *

from logging import getLogger
REG = getLogger(__name__)

from threading import Thread
from time import sleep
from pickle import dumps, loads
from random import choice
from os import path

from PodSixNet.Server import Server
from PodSixNet.Channel import Channel

from TE.faltas import *
from TE.recursos import obtenerNivel
from TE.reglas import Reglas, Jugador, crearPersonaje
from TE.juego import Juego, Personaje
from TE.mapa import Mapa, Pos
from TE.cartas import Carta, MasoCartas, BuscarCarta


class Canal(Channel):

    def Network_EnviarJugador(self, datos):
        self.jugador = datos['judador']
        REG.debug('Creando cliente {}'.format(hash(self.jugador)))

    def Network_RecivirAccion(self, datos):
        accion = loads(datos['accion'])
        REG.info('recibiendo jugadas de {}'.format(accion.personaje))
        try:
            self._server.reglas.procesarJugadas(accion)
        except Falta as err:
            REG.info('Accion invalida debido a {}'.format(err))
            self._server.enviarEstado(self)
        else:
            REG.info('Accion aceptada, transmitiendo a todos')
            self._server.TransmitirAccion(accion)

    def Network_disconnected(self, datps):
        REG.info('Cliente {} se desconecto'.format(self.jugador))


class Servidor(Thread, Server):

    channelClass = Canal
    IP = "127.0.0.1"
    PUERTO = 5071

    def __init__(self, jugadoresMaximos=2, archivoNivel='Mapa1'):
        """Crea un Servidor en Localhost que espera hasta tener"""
        Thread.__init__(self, name='Servidor')
        Server.__init__(self,
            channelClass=Canal,
            localaddr=(self.IP, self.PUERTO),
            listeners=jugadoresMaximos)
        self.jugadoresMaximos = jugadoresMaximos
        self.archivoNivel = archivoNivel
        self.canales = []
        REG.info("INICIANDO EL SERVIDOR EN {}:{}".format(self.IP, self.PUERTO))

    def run(self):
        Thread(target=self.esperarJugadores).start()
        REG.info("Inciciado bucle principal")
        while True:
            self.buclePrincipal()

    def buclePrincipal(self):
        self.Pump()
        sleep(1/1000)

    # Llamadas de Red
    def Connected(self, canal, direccion):
        REG.debug('Intento de coneccion de {}'.format(direccion))
        if len(self.canales) < self.jugadoresMaximos:
            self.canales.append(canal)
            REG.info('Nuevo jugador {}, ya tenemos {} de {}'.format(
                canal,
                len(self.canales),
                self.jugadoresMaximos))
        else:
            m = {
                'action' : 'error',
                'error' : 'El Servidor esta lleno',
                }
            canal.Send(m)

    def enviarEstado(self, canal, tipo=''):
        REG.debug('Enviando estado forzado tipo {} a {}'.format(tipo, canal))
        m = {
            'action':'ActualizarJuego',
            'reglas':dumps(self.reglas),
            'tipo':tipo,
            }
        canal.Send(m)

    def TransmitirTerminado(self, ganador):
        REG.info('Juego terminado, gano {}'.format(ganador))
        for canal in self.canales:
            self.enviarEstado(canal, 'terminado')

    def TransmitirTurno(self):
        REG.info('Turno {}'.format(self.juego.numeroTurno))
        for canal in self.canales:
            self.enviarEstado(canal, 'turno')

    def TransmitirPeriodo(self):
        REG.info('Periodo {}'.format(self.juego.numeroPeriodo))
        for canal in self.canales:
            self.enviarEstado(canal, 'periodo')

    def enviarAccion(self, canal, accion):
        m = {
            'action':'RecivirAccion',
            'accion':dumps(accion),
            }
        canal.Send(m)

    def RetransmitirAccion(self, canalOrigen, accion):
        for canal in self.canales:
            if not canal is canalOrigen:
                self.enviarAccion(canal, accion)

    def TransmitirAccion(self, accion):
        for canal in self.canales:
            self.enviarAccion(canal, accion)

    def PedirJugadas(self, canal, personajes):
        if personajes:
            m = {
                'action':'PedirJugadas',
                'personajes':dumps(personajes),
                }
            canal.Send(m)


    # Prcesos del juego
    def esperarJugadores(self):
        REG.info('Esperando a los jugadores')
        while True:
            sleep(1/1000)
            if len(self.canales) < self.jugadoresMaximos:
                continue
            for canal in self.canales:
                if not hasattr(canal, 'jugador'):
                    break
            else:
                break
        REG.info('Ya tenemos {} jugadores, iniciando el juego'.format(len(self.canales)))
        self.iniciarJuego()

    def iniciarJuego(self):
        self.cargarNivel()
        jugadores = [canal.jugador for canal in self.canales]
        personajes = []
        llenos = []
        for jugador in jugadores:
            p = []
            for personaje in jugador.personajes:
                #agregar hash del personaje al jugador
                n = jugador.personajes.index(personaje)
                personaje = crearPersonaje(personaje)
                p.append(personaje)
                jugador.personajes[n] = hash(personaje)
                #colocar personajes aleatoriamente
                puestos = [
                    pos for pos, objeto
                    in list(self.mapa.items())
                    if (pos.z == 0 and
                        objeto.caminable and
                        not pos in llenos
                        )
                    ]
                pos = choice(puestos)
                llenos.append(pos)
                pos = (pos.x, pos.y)
                self.mapa.colocar(personaje, pos)
                REG.info('Colocando {} en {}'.format(personaje, pos))
            personajes.extend(p)
        self.juego = Juego(self.mapa, personajes)
        self.reglas = Reglas(self.juego, jugadores)

        while True:
            terminado = self.reglas.terminado()
            if terminado:
                self.TransmitirTerminado(terminado)
                break
            self.turno()

    def cargarNivel(self):
        try:
            mapa = obtenerNivel(self.archivoNivel).get('Nivel', 'mapa')
            fondo = obtenerNivel(self.archivoNivel).get('Nivel', 'fondo')
        except IOError:
            linea1 = '_._._._.'
            linea2 = '._._._._'
            mapa = '\n'.join((linea1, linea2)*4)
            fondo = 'Ajedrez'

        self.mapa = Mapa(mapa, fondo)
        REG.info(str(self.mapa))

    # cliclo de juego
    def turno(self):
        self.reglas.turno()
        periodo = self.reglas.personajesTurno()
        self.TransmitirTurno()
        for personajes in periodo:
            self.periodo(personajes)

    def periodo(self, personajes):
        self.reglas.periodo()
        personajes = self.reglas.personajesPeriodo(personajes)
        self.TransmitirPeriodo()
        for canal in self.canales:
            p = []
            for personaje in personajes:
                if personaje in canal.jugador.personajes:
                    p.append(personaje)
            self.PedirJugadas(canal, p)
        while not self.reglas.periodoTerminado():
            sleep(1/1000)
        REG.info('Todos los personjes requeridos tomaron sus acciones')


def inciarServidor(jug, nivel):
    servidor = Servidor(jug, nivel)
    servidor.start()
    servidor.join()

def main():
    if len(sys.argv) != 3:
        print(("Uso:", sys.argv[0], "Jugadores Mapa"))
        print(("e.g.", sys.argv[0], "2 Nivel1"))
    else:
        jug, nivel = sys.argv[1:3]
        inciarServidor(int(jug), nivel)

if __name__ == '__main__':
    main()
