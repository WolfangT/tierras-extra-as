#!/usr/bin/env python
# -*- coding: utf-8 -*-

from TE.globales import *
from TE.faltas import *

"""Efecto dañar 1"""

variables = {'objetivo'}
duracion = PERIODO

costos = {'Resistencia':1}
extras = {'Rango':1}

def efecto(jugador, personaje, reglas, valores):
    objetivo = reglas.juego.mapa[valores['objetivo']]
    personaje = reglas.juego.buscarPersonaje(objetivo)
    try:
        personaje.destruirRecursos('Resistencia', 1)
    except AttributeError:
        pass
    except NoSuficientesRecursos:
        personaje.activo = False
