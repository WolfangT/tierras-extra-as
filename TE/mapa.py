#!/usr/bin/env python
# -*- coding: utf-8 -*-

from TE.globales import *

from logging import getLogger
REG = getLogger(__name__)

from uuid import uuid4
from os import path
from configparser import ConfigParser, NoOptionError

from pygame import image, transform, sprite

from TE.faltas import *
from TE.recursos import obtenerConf


MAPEO_MAPA_OBJETOS = {}
for seccion in obtenerConf('Objetos.conf').sections():
    MAPEO_MAPA_OBJETOS[seccion] = dict(obtenerConf('Objetos.conf').items(seccion))


class MapaBase(dict):
    """Clase base para mapas, mapea objetos a sus cordenadas"""

    def __init__(self, *posiciones):
        dict.__init__(self)
        for pos in posiciones:
            self[pos] = None

    def __getitem__(self, llave):
        if type(llave) is Pos:
            return dict.__getitem__(self, llave)
        else:
            raise TypeError('La llave debe se de tipo Pos')

    def __setitem__(self, llave, valor):
        if type(llave) is Pos:
            dict.__setitem__(self, llave, valor)
        else:
            raise TypeError('La llave debe se de tipo Pos')

    def __delitem__(self, llave):
        if type(llave) is Pos:
            return dict.__delitem__(self, llave)
        else:
            raise TypeError('La llave debe se de tipo Pos')

    def lista(self):
        """Regresa los contenidos del mapa como una lista ordenada"""
        fila = []
        for x in range(self.totX):
            columna = []
            for y in range(self.totY):
                viga = []
                for z in range(self.totZ):
                    viga.append(None)
                columna.append(viga)
            fila.append(columna)
        for pos, objeto in list(self.items()):
            fila[pos.x][pos.y][pos.z] = (pos, objeto)
        r = []
        for x in fila:
            for y in x:
                for z in y:
                    if not z is None:
                        r.append(z)
        return r

    @property
    def totX(self):
        return max([pos.x for pos, objeto in list(self.items())]) + 1

    @property
    def totY(self):
        return max([pos.y for pos, objeto in list(self.items())]) + 1

    @property
    def totZ(self):
        return max([pos.z for pos, objeto in list(self.items())]) + 1

    def buscar(self, objeto):
        """busca por toda las ubicaciones a un objeto"""
        for pos, o in list(self.items()):
            if hash(objeto) == hash(o):
                return pos
        else:
            raise KeyError('No se encontro el objeto dentro del mapa')


class Mapa(MapaBase):
    """Tipo de mapa especial para el juego, basado en un dibujo"""

    def __init__(self, ascii, fondo=None):
        """Carga un dibujo ascii del mapa y crea el mapa segun la configuracion"""
        MapaBase.__init__(self)
        self.ascii = ascii
        self.fondo = fondo
        #cargar imagen mapa
        posiciones = {}
        lineas = ascii.split('\n')
        for y, linea in enumerate(lineas):
            linea = linea[1:-1]
            for x, caracter in enumerate(linea):
                if caracter == ' ':
                    continue
                else:
                    icono = MAPEO_MAPA_OBJETOS[caracter]
                posiciones[Pos(x,y,0)] = icono
        for pos, icono in list(posiciones.items()):
            self[pos] = Estatico(**icono)

    def __str__(self):
        return self.ascii

    def obtenerPersonaje(self, pos):
        """Asume que todos los personajes entan en el piso 1"""
        if type(pos) is Pos:
            pos.z = 1
        else:
            pos = Pos(pos[0], pos[1], 1)
        try:
            return self[pos]
        except KeyError:
            return None

    def colocar(self, personaje, pos):
        """Asume que quieres agregar un personaje en el piso 1"""
        if type(pos) is Pos:
            pos.z = 1
        else:
            pos = Pos(pos[0], pos[1], 1)
        self[pos] = Movil(personaje)

    def quitar(self, pos):
        """Asume que quieres quitar un personaje en el piso 1"""
        if type(pos) is Pos:
            pos.z = 1
        else:
            pos = Pos(pos[0], pos[1], 1)
        self[pos] = None

    def mover(self, personaje, pos):
        """Busca al personaje y lo coloca en pos"""
        if type(pos) is Pos:
            pos.z = 1
        else:
            pos = Pos(pos[0], pos[1], 1)
        llave = self.buscar(personaje)
        p = self[llave]
        self[llave] = None
        p.pos = pos
        self[pos] = p

    def obtenerFondo(self, pos):
        """Asume que todos los fondos(Bloques) entan en el piso 0"""
        if type(pos) is Pos:
            pos.z = 0
        else:
            pos = Pos(pos[0], pos[1], 0)
        try:
            return self[pos]
        except KeyError:
            return None

    def caminable(self, pos):
        """Determina si un personaje puede caminar por el lugar"""
        try:
            frente = self[pos]
        except KeyError:
            frente = None
        p = pos - Pos(0,0,1)
        try:
            piso = self[p]
        except KeyError:
            piso = None
        return ((piso and piso.caminable) and
            (not frente or not frente.bloqueo))

    def obtenerAlrededor(self, pos):
        m = {
            #~ SO : Pos(-1,1),
            S : Pos(0,1),
            #~ SE : Pos(1,1),
            O : Pos(-1,0),
            E : Pos(1,0),
            #~ NO : Pos(-1,-1),
            N : Pos(0,-1),
            #~ NE : Pos(1,-1),
            }
        r = {}
        for d, p in list(m.items()):
            r[d] = self[pos+p] if p+pos in self else None
        return r


class Pos(object):
    """Objeto que representa una posicion en X, Y y Z, llave de Mapa"""

    def __init__(self, x=0, y=0, z=0):
        self.x = x
        self.y = y
        self.z = z

    _x = 0

    @property
    def x(self):
        return self._x

    @x.setter
    def x(self, valor):
        self._x = valor

    _y = 0
    @property
    def y(self):
        return self._y

    @y.setter
    def y(self, valor):
        self._y = valor

    _z = 0

    @property
    def z(self):
        return self._z

    @z.setter
    def z(self, valor):
        self._z = valor

    def __eq__(self, objeto):
        try:
            if ((objeto.x == self.x) and
                (objeto.y == self.y) and
                (objeto.z == self.z)):
                return True
            else:
                return False
        except AttributeError:
            return NotImplemented

    def __ne__(self, objeto):
        return not self.__eq__(objeto)

    def __bool__(self):
        return (self.x is not None) and (self.y is not None) and (self.z is not None)

    def __hash__(self):
        return int(str(id(self.x))+str(id(self.y))+str(id(self.z)))

    def __str__(self):
        return "({},{},{})".format(self.x,self.y,self.z)

    def __repr__(self):
        return  "Pos(x={}, y={}, z={})".format(self.x, self.y, self.z)

    def __add__(self, valor):
        """Pos cuyo X y Y son la suma de las Pos"""
        return Pos(x=(self.x+valor.x), y=(self.y+valor.y), z=(self.z+valor.z))

    def __radd__(self, valor):
        return self.__add__(valor)

    def __iadd__(self, valor):
        """Aumenta los X y Y pór los del valor"""
        self.x += valor.x
        self.y += valor.y
        self.z += valor.z
        return self

    def __sub__(self, valor):
        """Pos cuyo X y Y son la resta de las Pos"""
        return Pos(x=(self.x-valor.x), y=(self.y-valor.y), z=(self.z-valor.z))

    def __rsub__(self, valor):
        return self.__sub__(valor)

    def __isub__(self, valor):
        """Disminuye los X y Y por los del valor"""
        self.x -= valor.x
        self.y -= valor.y
        self.z -= valor.z
        return self

    def __int__(self):
        return abs(self.x) + abs(self.y) + abs(self.z)

    def __float__(self):
        return (self.x**2 + self.y**2 + self.z**2)**(0.5)


class Objeto(object):
    """Clase que representa un objeto fisico, contenido basico del Mapa"""

    def __init__(self, nombre, caminable, altura, bloqueo):
        self.nombre = nombre
        self.caminable = bool(int(caminable))
        self.altura = bool(int(altura))
        self.bloqueo = bool(int(bloqueo))
        self._uuid = int(uuid4())

    def __hash__(self):
        return self._uuid


class Movil(Objeto):
    """Clase que representa algo que cambia su pocicion"""

    caminable = False
    altura = False
    bloqueo = True

    def __init__(self, personaje):
        self.nombre = personaje.nombre
        self.imagen = '{}.png'.format(personaje.nombre)
        self._uuid = personaje._uuid


class Estatico(Objeto):
    """Tipo de Objeto que representa un bloque que no se puede mover"""
    pass
