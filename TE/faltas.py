#!/usr/bin/env python
# -*- coding: utf-8 -*-

class Falta(Exception):
    """Error base para faltas de las reglas del juego"""
    pass

class LimiteReacurso(Falta):
    """Limite de recursos de un tipo alcansado"""
    pass

class NoSuficientesRecursos(Falta):
    """No hay suficientes recursos de un tipo"""
    pass

class PersonajeOcupado(Falta):
    """Personaje esta ocupado"""
    pass

class NoSuficienteRango(Falta):
    """La distancia es mayor que el Rango permitido"""
    pass

class NoSuficienteNivel(Falta):
    """El recurso es de un nivel mayor al del personaje"""
    pass
