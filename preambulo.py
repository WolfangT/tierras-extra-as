#!python3.4-32
# modificar path para encontrar los recursos
import sys
pkgdir = sys.path[0]
maindir = sys.path[1]
sys.path[0] = maindir
sys.path[1] = pkgdir
# crear sistema de logging
from logging import StreamHandler, FileHandler, getLogger, DEBUG
from logging.handlers import SocketHandler, DEFAULT_TCP_LOGGING_PORT
root = getLogger('')
root.setLevel(DEBUG)
socketHandler = SocketHandler('localhost', DEFAULT_TCP_LOGGING_PORT)
root.addHandler(socketHandler)
fileHandler = FileHandler('registro.log', encoding='UTF-8')
root.addHandler(fileHandler)
streamHandler = StreamHandler()
root.addHandler(streamHandler)
