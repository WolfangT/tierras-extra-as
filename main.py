#!/usr/bin/env python
# -*- coding: utf-8 -*-

from logging import StreamHandler, FileHandler, getLogger, DEBUG
from logging.handlers import SocketHandler, DEFAULT_TCP_LOGGING_PORT
root = getLogger('')
root.setLevel(DEBUG)
socketHandler = SocketHandler('localhost', DEFAULT_TCP_LOGGING_PORT)
root.addHandler(socketHandler)
fileHandler = FileHandler('registro.log', encoding='UTF-8')
root.addHandler(fileHandler)
streamHandler = StreamHandler()
root.addHandler(streamHandler)

from TE.main import main

if __name__ == '__main__':
    main()
